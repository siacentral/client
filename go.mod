module gitlab.com/siacentral/client

go 1.12

require (
	github.com/StackExchange/wmi v0.0.0-20181212234831-e0a55b97c705 // indirect
	github.com/go-ole/go-ole v1.2.4 // indirect
	github.com/google/gopacket v1.1.17
	github.com/shirou/gopsutil v2.18.12+incompatible
	gitlab.com/siacentral/siaapi v0.2.1
	gitlab.com/siacentral/siadaemon v0.1.1
	golang.org/x/crypto v0.0.0-20190513172903-22d7a77e9e5f
	golang.org/x/net v0.0.0-20190514140710-3ec191127204 // indirect
	golang.org/x/sys v0.0.0-20190516110030-61b9204099cb // indirect
	golang.org/x/text v0.3.2 // indirect
	golang.org/x/tools v0.0.0-20190517003510-bffc5affc6df // indirect
)
