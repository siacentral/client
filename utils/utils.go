package utils

import (
	"archive/zip"
	"bytes"
	"crypto"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"encoding/json"
	"encoding/pem"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"math"
	"net/http"
	"os"
	"path/filepath"
	"runtime"
	"strings"
	"time"

	"gitlab.com/siacentral/client/consts"
	"gitlab.com/siacentral/client/types"
)

var (
	//ErrNoPublicKey indicates that the parser was unable to determine what the host's public key is
	ErrNoPublicKey = errors.New("no public key in host.json")
)

//MinInt Gets the smallest int passed in args
func MinInt(n ...int) int {
	min := n[0]

	for _, i := range n {
		if i > min {
			continue
		}

		min = i
	}

	return min
}

//MinUInt64 Gets the smallest uint64 passed in args
func MinUInt64(n ...uint64) uint64 {
	min := n[0]

	for _, i := range n {
		if i > min {
			continue
		}

		min = i
	}

	return min
}

//UnmarshalMap converts a map[string]interface{} into the output struct using JSON as the intermediary
func UnmarshalMap(data interface{}, output interface{}) (err error) {
	buf, err := json.Marshal(data)

	if err != nil {
		return
	}

	err = json.Unmarshal(buf, output)

	return
}

//GetNearestTime Gets the nearest time from the current time
func GetNearestTime(date time.Time, coefficient time.Duration) time.Time {
	current := float64(date.UnixNano())
	current = math.Ceil(current/float64(coefficient)) * float64(coefficient)

	return time.Unix(0, int64(current))
}

// GetLatestSiaVersion returns the latest Sia release
func GetLatestSiaVersion() (version types.SemVer, err error) {
	var releaseMeta []map[string]interface{}

	resp, err := http.Get("https://gitlab.com/api/v4/projects/7508674/repository/tags?order_by=name")

	if err != nil {
		return
	}

	defer resp.Body.Close()

	if err = json.NewDecoder(resp.Body).Decode(&releaseMeta); err != nil {
		return
	}

	if len(releaseMeta) == 0 {
		err = errors.New("no releases")
		return
	}

	version, err = types.ParseSemVer(releaseMeta[0]["name"].(string))

	return
}

//GetHostPublicKey parses the host config file and returns the public key in string format
func GetHostPublicKey(path string) (pubKey string, err error) {
	f, err := os.Open(path)

	if err != nil {
		return
	}

	defer f.Close()

	dec := json.NewDecoder(f)

	for {
		var tmp map[string]interface{}

		if err = dec.Decode(&tmp); err == io.EOF {
			err = ErrNoPublicKey
			break
		} else if err != nil {
			continue
		}

		if _, exists := tmp["publickey"]; !exists {
			continue
		}

		tmp, ok := tmp["publickey"].(map[string]interface{})

		if !ok {
			err = ErrNoPublicKey
			return
		}

		var algorithm, key string

		if algo, exists := tmp["algorithm"]; exists {
			algorithm = fmt.Sprintf("%s", algo)
		}

		if k, exists := tmp["key"]; exists {
			key = fmt.Sprintf("%s", k)
		}

		if len(algorithm) == 0 || len(key) == 0 {
			err = ErrNoPublicKey
			return
		}

		pubKey = fmt.Sprintf("%s:%s", algorithm, key)

		break
	}

	return
}

// defaultSiaDir returns the default data directory of siad. The values for
// supported operating systems are:
//
// Linux:   $HOME/.sia
// MacOS:   $HOME/Library/Application Support/Sia
// Windows: %LOCALAPPDATA%\Sia
func DefaultSiaDir() string {
	switch runtime.GOOS {
	case "windows":
		return filepath.Join(os.Getenv("LOCALAPPDATA"), "Sia")
	case "darwin":
		return filepath.Join(os.Getenv("HOME"), "Library", "Application Support", "Sia")
	default:
		return filepath.Join(os.Getenv("HOME"), ".sia")
	}
}

func GetExecName(name string) string {
	switch strings.ToLower(runtime.GOOS) {
	case "windows":
		return name + ".exe"
	default:
		return name
	}
}

func saveZipFile(savePath string, file *zip.File) (digest []byte, err error) {
	zipped, err := file.Open()

	if err != nil {
		return
	}

	defer zipped.Close()

	f, err := os.Create(savePath)

	if err != nil {
		return
	}

	defer f.Close()

	tee := io.TeeReader(zipped, f)

	hasher := sha256.New()

	_, err = io.Copy(hasher, tee)

	digest = hasher.Sum(nil)

	return
}

func readZipFile(file *zip.File) (buf []byte, err error) {
	zipped, err := file.Open()

	if err != nil {
		return
	}

	defer zipped.Close()

	buf, err = ioutil.ReadAll(zipped)

	return
}

//VerifySignature verifies the sha-256 hash matches the supplied signature signed with by the public key
func VerifySignature(digest, signature, publicKeyPEM []byte) (err error) {
	block, _ := pem.Decode(publicKeyPEM)

	if block == nil || block.Type != "PUBLIC KEY" {
		err = errors.New("unable to parse public key")
		return
	}

	pubKey, err := x509.ParsePKIXPublicKey(block.Bytes)

	if err != nil {
		return
	}

	err = rsa.VerifyPKCS1v15(pubKey.(*rsa.PublicKey), crypto.SHA256, digest, signature)

	return
}

//DownloadSiaDaemon downloads the latest version of siad for the current os and verifies the included signature
func DownloadSiaDaemon(downloadPath string) (installPath string, err error) {
	var siadDigest []byte
	var siadSig []byte

	defer func() {
		if err == nil || len(installPath) == 0 {
			return
		}

		//try to remove the installed siad since we errored out
		err := os.Remove(installPath)

		if err != nil && err != os.ErrNotExist {
			log.Println(err)
		}
	}()

	if runtime.GOARCH != "amd64" {
		err = errors.New("unknown architecture automatic update only works for amd64 based computers please set siad_path in config.json")
		return
	}

	currentVersion, err := GetLatestSiaVersion()

	if err != nil {
		return
	}

	downloadURL := fmt.Sprintf("https://sia.tech/releases/Sia-%s-%s-amd64.zip", currentVersion.String(), strings.ToLower(runtime.GOOS))

	resp, err := http.Get(downloadURL)

	if err != nil {
		return
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return
	}

	reader, err := zip.NewReader(bytes.NewReader(body), int64(len(body)))

	if err != nil {
		return
	}

	for _, file := range reader.File {
		name := filepath.Base(file.Name)

		switch name {
		case GetExecName("siad"):
			installPath = filepath.Join(downloadPath, GetExecName("siad"))
			if siadDigest, err = saveZipFile(installPath, file); err != nil {
				return
			}
		case GetExecName("siad") + ".sig":
			if siadSig, err = readZipFile(file); err != nil {
				return
			}
		}

		if len(siadDigest) > 0 && len(siadSig) > 0 {
			break
		}
	}

	if len(siadDigest) == 0 {
		err = errors.New("siad not in downloaded zip file")
		return
	}

	if len(siadSig) == 0 {
		err = errors.New("siad signature not in downloaded zip file")
	}

	err = VerifySignature(siadDigest, siadSig, consts.SiaSigningKey)

	return
}
