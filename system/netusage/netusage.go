package netusage

import (
	"net"
	"sync"
	"time"

	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
	"github.com/google/gopacket/pcap"
	"github.com/google/gopacket/tcpassembly"
)

var (
	statLock         sync.Mutex
	ipNetworkUsage   = make(map[string]*IPUsageStat)
	portNetworkUsage = make(map[string]*PortUsageStat)
)

type (
	//NetworkUsageStat wrapper to hold the total number of bytes sent and received across both ports
	NetworkUsageStat struct {
		Total uint64
		Sent  uint64
		Recv  uint64
	}

	IPUsageStat struct {
		NetworkUsageStat
		Address          string
		Ports            map[string]*PortUsageStat
		Sent, Rec, Total uint64
	}

	//PortUsageStat PortUsageStat
	PortUsageStat struct {
		NetworkUsageStat
		Port string
	}

	// usageStreamFactory counts the total number of bytes in the stream implements tcpassembly.StreamFactory
	usageStreamFactory struct{}

	// usageStream keeps track of the total number of bytes decoded
	usageStream struct {
		net, transport gopacket.Flow
	}
)

func addIPStats(ipaddr, port string, sent, recv uint64) {
	if _, exists := ipNetworkUsage[ipaddr]; !exists {
		ipNetworkUsage[ipaddr] = &IPUsageStat{
			Ports: make(map[string]*PortUsageStat),
		}
	}

	if _, exists := ipNetworkUsage[ipaddr].Ports[port]; !exists {
		ipNetworkUsage[ipaddr].Ports[port] = &PortUsageStat{}
	}

	ipNetworkUsage[ipaddr].Sent += sent
	ipNetworkUsage[ipaddr].Recv += recv
	ipNetworkUsage[ipaddr].Total += sent + recv

	ipNetworkUsage[ipaddr].Ports[port].Sent += sent
	ipNetworkUsage[ipaddr].Ports[port].Recv += recv
	ipNetworkUsage[ipaddr].Ports[port].Total += sent + recv
}

func addPortStats(port string, sent, recv uint64) {
	if _, exists := portNetworkUsage[port]; !exists {
		portNetworkUsage[port] = &PortUsageStat{}
	}

	portNetworkUsage[port].Sent += sent
	portNetworkUsage[port].Recv += recv
	portNetworkUsage[port].Total += sent + recv
}

func addRecvStats(ipaddr, port string, count uint64) {
	statLock.Lock()

	defer statLock.Unlock()

	if _, exists := ipNetworkUsage[ipaddr]; !exists {
		ipNetworkUsage[ipaddr] = &IPUsageStat{
			Ports: make(map[string]*PortUsageStat),
		}
	}

	if _, exists := ipNetworkUsage[ipaddr].Ports[port]; !exists {
		ipNetworkUsage[ipaddr].Ports[port] = &PortUsageStat{}
	}

	if _, exists := portNetworkUsage[port]; !exists {
		portNetworkUsage[port] = &PortUsageStat{}
	}

	ipNetworkUsage[ipaddr].Recv += count
	ipNetworkUsage[ipaddr].Total += count
	ipNetworkUsage[ipaddr].Ports[port].Sent += count

	portNetworkUsage[port].Recv += count
	portNetworkUsage[port].Total += count
}

// New creates a new stream.  It's called whenever the assembler sees a stream
// it isn't currently following.
func (factory *usageStreamFactory) New(net, transport gopacket.Flow) tcpassembly.Stream {
	s := &usageStream{
		net:       net,
		transport: transport,
	}

	// ReaderStream implements tcpassembly.Stream, so we can return a pointer to it.
	return s
}

//returns true if the ip address provided is an internal ip address
func checkInternal(dst string) bool {
	destIP := net.ParseIP(dst)
	addrs, err := net.InterfaceAddrs()

	if err != nil {
		panic(err)
	}

	for _, addr := range addrs {
		_, ip, err := net.ParseCIDR(addr.String())

		if err != nil {
			panic(err)
		}

		if ip.Contains(destIP) {
			return true
		}
	}

	return false
}

// Reassembled called when new packet data is available. Determines which counter to increment
func (s *usageStream) Reassembled(reassemblies []tcpassembly.Reassembly) {
	statLock.Lock()

	defer statLock.Unlock()

	for _, reassembly := range reassemblies {
		l := uint64(len(reassembly.Bytes))

		if checkInternal(s.net.Dst().String()) {
			p := s.transport.Dst().String()

			addIPStats(s.net.Dst().String(), p, 0, l)
			addIPStats(s.net.Src().String(), p, l, 0)
			addPortStats(p, 0, l)
		} else {
			p := s.transport.Src().String()

			addIPStats(s.net.Src().String(), p, 0, l)
			addIPStats(s.net.Dst().String(), p, l, 0)
			addPortStats(p, 0, l)
		}
	}
}

// ReassemblyComplete is called when the TCP assembler believes a stream has finished.
// Global counters are incremented in the Reassembled func, so nothing needs to be done here.
func (s *usageStream) ReassemblyComplete() {
	return
}

//GetIPUsageStats GetIPUsageStats
func GetIPUsageStats() (usage map[string]IPUsageStat) {
	statLock.Lock()

	defer statLock.Unlock()

	usage = make(map[string]IPUsageStat)

	for key, value := range ipNetworkUsage {
		if value == nil {
			continue
		}

		usage[key] = *value
	}

	return
}

//GetPortUsageStats GetPortUsageStats
func GetPortUsageStats() (usage map[string]PortUsageStat) {
	statLock.Lock()

	defer statLock.Unlock()

	usage = make(map[string]PortUsageStat)

	for key, value := range portNetworkUsage {
		if value == nil {
			continue
		}

		usage[key] = *value
	}

	return
}

func StartCapture() (err error) {
	ifaces, err := pcap.FindAllDevs()

	if err != nil {
		return
	}

	for _, iface := range ifaces {
		go func(iface string) {
			startCapture(iface)
		}(iface.Name)
	}

	return
}

//startCapture starts usage monitoring on all interfaces and ports
func startCapture(iface string) (err error) {
	flushDuration := time.Minute * 2
	handle, err := pcap.OpenLive(iface, 65536, true, flushDuration/2)

	if err != nil {
		return
	}

	if err = handle.SetBPFFilter("tcp"); err != nil {
		return
	}

	streamFactory := &usageStreamFactory{}
	streamPool := tcpassembly.NewStreamPool(streamFactory)
	assembler := tcpassembly.NewAssembler(streamPool)

	var eth layers.Ethernet
	var dot1q layers.Dot1Q
	var ip4 layers.IPv4
	var ip6 layers.IPv6
	var ip6extensions layers.IPv6ExtensionSkipper
	var tcp layers.TCP
	var payload gopacket.Payload
	parser := gopacket.NewDecodingLayerParser(layers.LayerTypeEthernet,
		&eth, &dot1q, &ip4, &ip6, &ip6extensions, &tcp, &payload)
	decoded := make([]gopacket.LayerType, 0, 4)

	nextFlush := time.Now().Add(flushDuration / 2)

loop:
	for {
		if time.Now().After(nextFlush) {
			assembler.FlushOlderThan(time.Now().Add(flushDuration))
			nextFlush = time.Now().Add(flushDuration / 2)
		}

		data, ci, err := handle.ZeroCopyReadPacketData()

		if err != nil {
			continue
		}

		err = parser.DecodeLayers(data, &decoded)
		if err != nil {
			continue
		}

		var foundNetLayer bool
		var netFlow gopacket.Flow

		for _, typ := range decoded {
			switch typ {
			case layers.LayerTypeIPv4:
				netFlow = ip4.NetworkFlow()
				foundNetLayer = true
			case layers.LayerTypeIPv6:
				netFlow = ip6.NetworkFlow()
				foundNetLayer = true
			case layers.LayerTypeTCP:
				if foundNetLayer {
					assembler.AssembleWithTimestamp(netFlow, &tcp, ci.Timestamp)
				}

				continue loop
			}
		}
	}
}
