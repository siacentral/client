# About SiaCentral

## What is SiaCentral?

SiaCentral is a secure centralized web portal where you can manage and monitor your host nodes on the Sia Network. This client daemon starts and monitors the Sia Daemon on your server and facilitates secure end-to-end encrypted communication between your host and the SiaCentral server.

### Features

+ Change pricing instantly from anywhere
+ Restart your daemon
+ Announce your host on the Sia Network
+ Instant Notifications for status events
	+ Host goes offline
	+ Not enough collateral budget
	+ Storage space full
	+ IP address changed
	+ Sia out of date
	+ Sia crashed

## FAQ

### Will I need to download Sia?

Yes.

This client acts as a data exchange between your host and our server. You will need to have the latest version of Sia installed and synced before you can use our service.

### Can SiaCentral steal my coins?

No.

SiaCentral does not have access to any coins stored in your wallet. Feel free to browse through our source code.

### What information do you store?

For each host we store basic information about the daemon and update it every 5 minutes:

+ block chain synced
+ current block height
+ accepting contracts
+ siad version
+ last block hash
+ host working status
+ difficulty
+ wallet confirmed balance
+ contract compensation
+ number of storage obligations
+ locked collateral
+ risked collateral
+ configured storage price
+ used storage capacity
+ remaining storage capacity

We do this so we can provide useful historical data even when your host is offline.

### Can anyone send commands to my host?

No.

When a host is added to our service two keys are generated. All commands issued to your client must be signed by the private key unique to each individual host. If a message is not signed it is ignored and the connection is immediately dropped. Only you have access to your command keys no one else can access data from your host.

### Do I need to forward ports?

Generally no.

Your Sia daemon connects to our service using tls over tcp. As long as outbound traffic to siacentral.com is allowed our client should not need any forwarded ports.

### Is SiaCentral open source?

Not initially, hopefully in the future.

Initially, I plan to provide the full source code only to the SiaCentral client for transparency about what data is sent/received by connected sia daemons. 

The web-ui and backend server will remain closed source for the time being. 