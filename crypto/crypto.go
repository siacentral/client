package crypto

import (
	cryptoRand "crypto/rand"
	"crypto/sha512"
	"encoding/base64"
	"encoding/json"
	"errors"

	"golang.org/x/crypto/nacl/box"
)

var (
	//ErrNoCommand indicates that the message received did not have the required command key
	ErrNoCommand = errors.New("received message missing command key")
	//ErrInvalidKey indicates that the message was not signed properly and should be rejected
	ErrInvalidKey = errors.New("invalid signing key")
)

//RandomBytes generates a random byte string of length n
func RandomBytes(n int) (bytes []byte, err error) {
	bytes = make([]byte, n)

	_, err = cryptoRand.Read(bytes)

	return
}

//RandomKey generates a random base64 encoded key of the specified byte size
func RandomKey(n int) (key string, err error) {
	keyBytes, err := RandomBytes(n)

	if err != nil {
		return
	}

	key = base64.StdEncoding.EncodeToString(keyBytes)

	return
}

//HashToString hashes the given bytes to a base64 encoded string
func HashToString(data []byte) (hashed string, err error) {
	hashedBytes, err := Hash(data)

	if err != nil {
		return
	}

	hashed = base64.StdEncoding.EncodeToString(hashedBytes)

	return
}

//Hash hashes the given bytes
func Hash(data []byte) (hashed []byte, err error) {
	hasher := sha512.New()

	if _, err = hasher.Write(data); err != nil {
		return
	}

	hashed = hasher.Sum(nil)

	return
}

//EncryptMessage signs the passed in interface using the specified key. Messages are currently encrypted using NaCL box.
//The first 64 bytes of each message is the sha-512 fingerprint of the recipient's public key, followed by a 24 byte nonce, the remainder is the encrypted data.
func EncryptMessage(message []byte, recipPubKey, senderPrivKey *[32]byte) (data []byte, err error) {
	var nonce [24]byte

	nonceBytes, err := RandomBytes(24)

	if err != nil {
		return
	}

	copy(nonce[:], nonceBytes)

	fingerprint, err := Hash((*recipPubKey)[:])

	if err != nil {
		return
	}

	encrypted := box.Seal(nonce[:], message, &nonce, recipPubKey, senderPrivKey)

	data = append(fingerprint, encrypted...)

	return
}

//ParseMessage Parses a signed message to the specified interface. Returns an error if the message is not signed properly
func ParseMessage(message []byte, senderPubKey, recipPrivKey *[32]byte, data interface{}) (err error) {
	var nonce [24]byte

	copy(nonce[:], message[64:88])

	decrypted, success := box.Open(nil, message[88:], &nonce, senderPubKey, recipPrivKey)

	if !success {
		err = ErrInvalidKey
		return
	}

	err = json.Unmarshal(decrypted, &data)

	return
}
