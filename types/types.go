package types

const (
	//ResponseStatusSuccess indicates the message was handled successfully
	ResponseStatusSuccess = ResponseStatus("success")
	//ResponseStatusErr indicates an error in processing the message
	ResponseStatusErr = ResponseStatus("error")

	DaemonStatusStopped = DaemonStatus("stopped")
	DaemonStatusStarted = DaemonStatus("started")
	DaemonStatusLoaded  = DaemonStatus("loaded")
	DaemonStatusRestart = DaemonStatus("restart")
	DaemonStatusCrashed = DaemonStatus("crashed")

	//MessageConnected simple connection handshake between client and server. Sent upon every connection with the server
	MessageConnected = MessageType("connected")
)

type (
	//DaemonStatus indicates the current status of the Sia Daemon
	DaemonStatus string

	//ResponseStatus indicates if the message was handled successfully
	ResponseStatus string

	//MessageType the command type that was issued by the user
	MessageType string

	//ResponseBase responses are sent from the client to the server. Usually used to respond to a command
	ResponseBase struct {
		Type          MessageType    `json:"message_type"`
		ID            string         `json:"id"`
		ClientVersion SemVer         `json:"client_version"`
		Message       string         `json:"message"`
		Status        ResponseStatus `json:"status"`
	}

	//RequestBase the base for all requests
	RequestBase struct {
		ID   string      `json:"id"`
		Type MessageType `json:"message_type"`
	}
)
