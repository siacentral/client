package types

import (
	"time"
)

const (
	//MessageDaemonState returns the current state of the Sia Daemon after it has changed due to a crash or exit
	MessageDaemonState = MessageType("daemon_state")

	//MessageDaemonStatus requests that the host respond with any output siad has sent to StdOut or StdErr
	MessageDaemonStatus = MessageType("daemon_status")

	//MessageDaemonError requests that the host respond with any output siad has sent to StdErr
	MessageDaemonError = MessageType("daemon_error")

	//MessageDaemonStart requests that the host restart the sia daemon
	MessageDaemonStart = MessageType("daemon_start")

	//MessageDaemonUpdate requests that the sia daemon update itself
	MessageDaemonUpdate = MessageType("daemon_update")
)

type (
	//DaemonStatusResponse contains output from the Sia Daemon
	DaemonStatusResponse struct {
		ResponseBase
		SessionID    string       `json:"session_id"`
		Output       []string     `json:"output"`
		Errors       []string     `json:"errors"`
		PID          int          `json:"pid"`
		Running      bool         `json:"running"`
		DaemonStatus DaemonStatus `json:"daemon_status"`
		StartTime    time.Time    `json:"uptime"`
	}
)