package types

import (
	siatypes "gitlab.com/siacentral/siaapi/types"
)

const (
	//MessageAddLocalFolder requests that the host create a local directory at the specified path
	MessageAddLocalFolder = MessageType("storage_add_local_folder")

	//MessageStorageListDir requests that the host provide a directory listing for the specified path
	MessageStorageListDir = MessageType("storage_list_dir")

	//MessageStorageListFolders requests that the host list all the storage folders currently created by Sia
	MessageStorageListFolders = MessageType("storage_list_folders")

	//MessageStorageAddFolder requests that the host announce the host to the Sia network
	MessageStorageAddFolder = MessageType("storage_add_folder")

	//MessageStorageResizeFolder requests that the host announce the host to the Sia network
	MessageStorageResizeFolder = MessageType("storage_resize_folder")

	//MessageStorageRemoveFolder requests that the host announce the host to the Sia network
	MessageStorageRemoveFolder = MessageType("storage_remove_folder")
)

type (
	//DirectoryResponse responds to a DirectoryRequest with the files in folders in the given directory
	DirectoryResponse struct {
		ResponseBase
		TotalBytes  uint64          `json:"total_bytes"`
		FreeBytes   uint64          `json:"free_bytes"`
		UsedBytes   uint64          `json:"used_bytes"`
		DirPath     string          `json:"dir_path"`
		ParentPath  string          `json:"parent_path"`
		Directories []DirectoryStat `json:"directories"`
	}

	//DirectoryRequest list the folders and files at the specified directory
	DirectoryRequest struct {
		RequestBase
		DirPath string `json:"dir_path"`
	}

	DirectoryStat struct {
		Name       string `json:"name"`
		Path       string `json:"path"`
		UsedBytes  uint64 `json:"used_bytes"`
		FreeBytes  uint64 `json:"free_bytes"`
		TotalBytes uint64 `json:"total_bytes"`
	}

	//FolderRequest handles all of the folder related requests
	FolderRequest struct {
		RequestBase
		Path string `json:"path"`
		Size uint64 `json:"size"`
	}

	StorageFolder struct {
		Path              string          `json:"path"`
		Capacity          siatypes.BigNumber `json:"capacity"`
		RemainingCapacity siatypes.BigNumber `json:"remaining_capacity"`
		FailedReads       uint64          `json:"failed_reads"`
		FailedWrites      uint64          `json:"failed_writes"`
		SuccessfulReads   uint64          `json:"successful_reads"`
		SuccessfulWrites  uint64          `json:"successful_writes"`
	}

	StorageFoldersResponse struct {
		ResponseBase
		Capacity          siatypes.BigNumber `json:"capacity"`
		RemainingCapacity siatypes.BigNumber `json:"remaining_capacity"`
		FolderCount       uint64          `json:"folder_count"`
		Folders           []StorageFolder `json:"folders"`
		ChangedFolder     StorageFolder   `json:"changed_folder"`
	}
)