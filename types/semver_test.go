package types_test

import (
	"testing"

	"gitlab.com/siacentral/client/types"
)

func TestParse(t *testing.T) {
	_, err := types.ParseSemVer("")

	if err == nil {
		t.Fail()
	}

	v1, err := types.ParseSemVer("v1.4.0-rc4")

	if err != nil {
		t.Error(err)
	}

	if v1.Major != 1 && v1.Minor != 4 && v1.Patch != 0 {
		t.Errorf("wanted v1, 4, 0 got %d, %d, %d", v1.Major, v1.Minor, v1.Patch)
	}

	v2, err := types.ParseSemVer("1.3.7")

	if err != nil {
		t.Error(err)
	}

	if v2.Major != 1 && v2.Minor != 3 && v2.Patch != 7 {
		t.Errorf("wanted v1, 3, 7 got %d, %d, %d", v2.Major, v2.Minor, v2.Patch)
	}

	if v1.Compare(v1) != 0 {
		t.Errorf("%s should be equal to %s", v1.String(), v1.String())
	}

	if v1.Compare(v2) != 1 {
		t.Errorf("%s should be greater than %s", v1.String(), v2.String())
	}

	if v2.Compare(v1) != -1 {
		t.Errorf("%s should be less than than %s", v2.String(), v1.String())
	}
}
