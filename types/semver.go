package types

import (
	"database/sql/driver"
	"errors"
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

var (
	reg = regexp.MustCompile(`[0-9]+\.[0-9]+\.[0-9]+`)

	//ErrInvalidSemVer ErrInvalidSemVer
	ErrInvalidSemVer = errors.New("invalid semver format")
)

type (
	//SemVer SemVer
	SemVer struct {
		Major uint `json:"major"`
		Minor uint `json:"minor"`
		Patch uint `json:"patch"`
	}
)

//ToString converts the struct into its string representation
func (v SemVer) String() string {
	return fmt.Sprintf("v%d.%d.%d", v.Major, v.Minor, v.Patch)
}

//ParseSemVer parses a semver string into a struct
func ParseSemVer(version string) (v SemVer, err error) {
	var major, minor, patch uint64

	version = reg.FindString(version)
	parts := strings.Split(version, ".")

	if len(parts) != 3 {
		err = ErrInvalidSemVer
		return
	}

	if major, err = strconv.ParseUint(parts[0], 10, 32); err != nil {
		return
	}

	if minor, err = strconv.ParseUint(parts[1], 10, 32); err != nil {
		return
	}

	if patch, err = strconv.ParseUint(parts[2], 10, 32); err != nil {
		return
	}

	v.Major = uint(major)
	v.Minor = uint(minor)
	v.Patch = uint(patch)

	return
}

// Compare compares Versions v to v2:
// -1 == v is less than v2
// 0 == v is equal to v2
// 1 == v is greater than v2
func (v SemVer) Compare(v2 SemVer) int {
	if v.Major < v2.Major {
		return -1
	} else if v.Major > v2.Major {
		return 1
	}

	if v.Minor < v2.Minor {
		return -1
	} else if v.Minor > v2.Minor {
		return 1
	}

	if v.Patch < v2.Patch {
		return -1
	} else if v.Patch > v2.Patch {
		return 1
	}

	return 0
}

//MarshalJSON implements the MarshalJSON function for JSON parsing
func (v SemVer) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf("\"v%d.%d.%d\"", v.Major, v.Minor, v.Patch)), nil
}

//UnmarshalJSON implements the UnmarshalJSON function for JSON parsing
func (v *SemVer) UnmarshalJSON(b []byte) (err error) {
	parsed, err := ParseSemVer(string(b))

	if err != nil {
		return
	}

	*v = parsed

	return
}

//Scan implements the sql.Scanner interface
func (v *SemVer) Scan(src interface{}) (err error) {
	parsed, err := ParseSemVer(src.(string))

	if err != nil {
		return
	}

	*v = parsed

	return nil
}

//Value implements the sql.Valuer interface
func (v SemVer) Value() (val driver.Value, err error) {
	return v.String(), nil
}
