package types

import (
	siatypes "gitlab.com/siacentral/siaapi/types"
)

const (
	//MessageHostMetrics requests that the host respond with details about the running Sia daemon
	MessageHostMetrics = MessageType("data_status")

	//MessageDataListContracts requests that the host list all the contracts currently formed by Sia
	MessageDataListContracts = MessageType("data_list_contracts")

	//MessageHostStatus requests that the host respond with basic status information
	MessageHostStatus = MessageType("host_get_status")

	//MessageHostConfig requests that the host respond with the current host configuration
	MessageHostConfig = MessageType("host_get_config")

	//MessageHostAvgConfig requests that the host respond with the current host configuration
	MessageHostAvgConfig = MessageType("host_get_avg_config")

	//MessageHostUpdateConfig requests that the host update the host configuration
	MessageHostUpdateConfig = MessageType("host_update_config")

	//MessageHostAnnounce requests that the host announce the host to the Sia network
	MessageHostAnnounce = MessageType("host_announce")
)

type (
	//AnnounceRequest request the host announce at the specified address
	AnnounceRequest struct {
		RequestBase
		Address string `json:"address"`
	}

	ContractsResponse struct {
		ResponseBase
		ContractCount      uint64        `json:"contract_count"`
		ChainContractCount uint64        `json:"chain_contract_count"`
		Contracts          []SiaContract `json:"contracts"`
	}

	//HostConfigResponse HostConfigResponse
	HostConfigResponse struct {
		ResponseBase
		AcceptingContracts   bool               `json:"accepting_contracts"`
		MaxDownloadBatchSize uint64             `json:"max_download_batch_size"`
		MaxDuration          uint64             `json:"max_duration"`
		MaxReviseBatchSize   uint64             `json:"max_revise_batch_size"`
		WindowSize           uint64             `json:"window_size"`
		NetAddress           string             `json:"net_address"`
		Collateral           siatypes.BigNumber `json:"collateral"`
		CollateralBudget     siatypes.BigNumber `json:"collateral_budget"`
		MaxCollateral        siatypes.BigNumber `json:"max_collateral"`
		MinContractPrice     siatypes.BigNumber `json:"min_contract_price"`
		MinDownloadPrice     siatypes.BigNumber `json:"min_download_price"`
		MinStoragePrice      siatypes.BigNumber `json:"min_storage_price"`
		MinUploadPrice       siatypes.BigNumber `json:"min_upload_price"`
	}

	//HostMetricsResponse contains basic status and metrics from the Sia Daemon
	HostMetricsResponse struct {
		ResponseBase
		Synced               bool               `json:"synced"`
		AcceptingContracts   bool               `json:"accepting_contracts"`
		WalletUnlocked       bool               `json:"wallet_unlocked"`
		OperatingSystem      string             `json:"operating_system"`
		NetAddress           string             `json:"net_address"`
		PublicKey            string             `json:"public_key"`
		SiaVersion           SemVer             `json:"sia_version"`
		LastBlock            string             `json:"last_block"`
		WorkingStatus        string             `json:"working_status"`
		HostPort             string             `json:"host_port"`
		RPCPort              string             `json:"rpc_port"`
		Difficulty           siatypes.BigNumber `json:"difficulty"`
		ConfirmedBalance     siatypes.BigNumber `json:"confirmed_balance"`
		ContractCompensation siatypes.BigNumber `json:"contract_compensation"`
		LockedCollateral     siatypes.BigNumber `json:"locked_collateral"`
		RiskedCollateral     siatypes.BigNumber `json:"risked_collateral"`
		MaxCollateral        siatypes.BigNumber `json:"max_collateral"`
		StoragePrice         siatypes.BigNumber `json:"storage_price"`
		Capacity             siatypes.BigNumber `json:"capacity"`
		RemainingCapacity    siatypes.BigNumber `json:"remaining_capacity"`
		RPCSent              siatypes.BigNumber `json:"rpc_sent"`
		RPCRecv              siatypes.BigNumber `json:"rpc_recv"`
		HostSent             siatypes.BigNumber `json:"host_sent"`
		HostRecv             siatypes.BigNumber `json:"host_recv"`
		BlockHeight          uint64             `json:"block_height"`
		ContractCount        uint64             `json:"contract_count"`
		Contracts            []SiaContract      `json:"contracts"`
	}

	//HostStatusResponse contains basic status information from the Sia Daemon
	HostStatusResponse struct {
		ResponseBase
		SiaVersion  SemVer `json:"sia_version"`
		BlockHeight uint64 `json:"block_height"`
		PublicKey   string `json:"public_key"`
		NetAddress  string `json:"net_address"`
		Version     SemVer `json:"version"`
		Synced      bool   `json:"synced"`
	}

	SiaContract struct {
		ObligationID             string             `json:"obligation_id"`
		ObligationStatus         string             `json:"obligation_status"`
		SectorRootsCount         uint64             `json:"sector_roots_count"`
		ContractCost             siatypes.BigNumber `json:"contract_cost"`
		DataSize                 siatypes.BigNumber `json:"data_size"`
		LockedCollateral         siatypes.BigNumber `json:"locked_collateral"`
		PotentialDownloadRevenue siatypes.BigNumber `json:"potential_download_revenue"`
		PotentialStorageRevenue  siatypes.BigNumber `json:"potential_storage_revenue"`
		PotentialUploadRevenue   siatypes.BigNumber `json:"potential_upload_revenue"`
		RiskedCollateral         siatypes.BigNumber `json:"risked_collateral"`
		TransactionFeesAdded     siatypes.BigNumber `json:"transaction_fees_added"`
		ExpirationHeight         siatypes.BigNumber `json:"expiration_height"`
		NegotiationHeight        siatypes.BigNumber `json:"negotiation_height"`
		ProofDeadline            siatypes.BigNumber `json:"proof_deadline"`
		OriginConfirmed          bool               `json:"origin_confirmed"`
		ProofConfirmed           bool               `json:"proof_confirmed"`
		ProofConstructed         bool               `json:"proof_constructed"`
		RevisionConfirmed        bool               `json:"revision_confirmed"`
		RevisionConstructed      bool               `json:"revision_constructed"`
		ExistsOnChain            bool               `json:"exists_on_chain"`
	}

	//UpdateHostConfigRequest UpdateHostConfigRequest
	UpdateHostConfigRequest struct {
		RequestBase
		AcceptingContracts   bool               `json:"accepting_contracts"`
		MaxDownloadBatchSize uint64             `json:"max_download_batch_size"`
		MaxDuration          uint64             `json:"max_duration"`
		MaxReviseBatchSize   uint64             `json:"max_revise_batch_size"`
		WindowSize           uint64             `json:"window_size"`
		NetAddress           string             `json:"net_address"`
		Collateral           siatypes.BigNumber `json:"collateral"`
		CollateralBudget     siatypes.BigNumber `json:"collateral_budget"`
		MaxCollateral        siatypes.BigNumber `json:"max_collateral"`
		MinContractPrice     siatypes.BigNumber `json:"min_contract_price"`
		MinDownloadPrice     siatypes.BigNumber `json:"min_download_price"`
		MinStoragePrice      siatypes.BigNumber `json:"min_storage_price"`
		MinUploadPrice       siatypes.BigNumber `json:"min_upload_price"`
	}
)
