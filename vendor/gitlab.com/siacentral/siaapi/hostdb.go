package siaapi

import (
	"gitlab.com/siacentral/siaapi/responses"
)

// GetHosts requests the /hostdb endpoint's resources.
func (c *Client) GetHosts() (hdg responses.HostScan, err error) {
	err = c.makeRequest(HTTPGet, "/hostdb", "", &hdg)
	return
}

// GetActiveHosts requests the /hostdb/active endpoint's resources.
func (c *Client) GetActiveHosts() (hdag responses.Hosts, err error) {
	err = c.makeRequest(HTTPGet, "/hostdb/active", "", &hdag)
	return
}

// GetAllHosts requests the /hostdb/all endpoint's resources.
func (c *Client) GetAllHosts() (hdag responses.Hosts, err error) {
	err = c.makeRequest(HTTPGet, "/hostdb/all", "", &hdag)
	return
}

// GetHostByKey request the /hostdb/hosts/:pubkey endpoint's resources.
func (c *Client) GetHostByKey(pk string) (hhg responses.HostDetails, err error) {
	err = c.makeRequest(HTTPGet, "/hostdb/hosts/"+pk, "", &hhg)
	return
}
