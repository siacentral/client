package siaapi

import (
	"fmt"
	"net/url"
	"strconv"

	"gitlab.com/siacentral/siaapi/responses"
)

// HostParam is a parameter in the host's settings that can be changed via the
// API. It is primarily used as a helper struct to ensure type safety.
type HostParam string

//HostConfig holds host parameters and values
type HostConfig map[HostParam]interface{}

//ToURLValues converts the host config into a url.Values map
func (config *HostConfig) ToURLValues() (params url.Values) {
	params = make(url.Values)

	for param, val := range *config {
		params[string(param)] = []string{fmt.Sprint(val)}
	}

	return
}

const (
	// HostParamCollateralBudget is the collateral budget of the host in
	// hastings.
	HostParamCollateralBudget = HostParam("collateralbudget")
	// HostParamMaxCollateral is the max collateral of the host in hastings.
	HostParamMaxCollateral = HostParam("maxcollateral")
	// HostParamMinContractPrice is the min contract price in hastings.
	HostParamMinContractPrice = HostParam("mincontractprice")
	// HostParamMinDownloadBandwidthPrice is the min download bandwidth price
	// in hastings/byte.
	HostParamMinDownloadBandwidthPrice = HostParam("mindownloadbandwidthprice")
	// HostParamMinUploadBandwidthPrice is the min upload bandwidth price in
	// hastings/byte.
	HostParamMinUploadBandwidthPrice = HostParam("minuploadbandwidthprice")
	// HostParamCollateral is the host's collateral in hastings/byte/block.
	HostParamCollateral = HostParam("collateral")
	// HostParamMinStoragePrice is the minimum storage price in
	// hastings/byte/block.
	HostParamMinStoragePrice = HostParam("minstorageprice")
	// HostParamMinBaseRPCPrice is the minimum base RPC price in hastings.
	HostParamMinBaseRPCPrice = HostParam("minbaserpcprice")
	// HostParamMinSectorAccessPrice is the minimum sector access price in
	// hastings.
	HostParamMinSectorAccessPrice = HostParam("minsectoraccessprice")
	// HostParamAcceptingContracts indicates if the host is accepting new
	// contracts.
	HostParamAcceptingContracts = HostParam("acceptingcontracts")
	// HostParamMaxDuration is the max duration of a contract in blocks.
	HostParamMaxDuration = HostParam("maxduration")
	// HostParamWindowSize is the size of the proof window in blocks.
	HostParamWindowSize = HostParam("windowsize")
	// HostParamMaxDownloadBatchSize is the maximum size of the download batch
	// size in bytes.
	HostParamMaxDownloadBatchSize = HostParam("maxdownloadbatchsize")
	// HostParamMaxReviseBatchSize is the maximum size of the revise batch size.
	HostParamMaxReviseBatchSize = HostParam("maxrevisebatchsize")
	// HostParamNetAddress is the announced netaddress of the host.
	HostParamNetAddress = HostParam("netaddress")
)

// AnnounceHost uses the /host/announce endpoint to announce the host to
// the network
func (c *Client) AnnounceHost() (err error) {
	_, err = c.makeRawRequest(HTTPPost, "/host/announce", "")

	return
}

// AnnounceHostAddr uses the /host/anounce endpoint to announce the host to
// the network using the provided address.
func (c *Client) AnnounceHostAddr(address string) (err error) {
	_, err = c.makeRawRequest(HTTPPost, "/host/announce", url.Values{
		"netaddress": []string{address},
	}.Encode())

	return
}

// GetHostContracts uses the /host/contracts endpoint to get information
// about contracts on the host.
func (c *Client) GetHostContracts() (cg responses.HostContracts, err error) {
	err = c.makeRequest(HTTPGet, "/host/contracts", "", &cg)

	return
}

// EstimateHostScore requests the /host/estimatescore endpoint.
func (c *Client) EstimateHostScore(config HostConfig) (eg responses.HostScoreEstimate, err error) {
	err = c.makeRequest(HTTPGet, "/host/estimatescore", config.ToURLValues().Encode(), &eg)

	return
}

// GetHostStats requests the /host endpoint.
func (c *Client) GetHostStats() (hg responses.Host, err error) {
	err = c.makeRequest(HTTPGet, "/host", "", &hg)

	return
}

// UpdateHostConfig uses the /host endpoint to change a param of the host
// settings to a certain value.
func (c *Client) UpdateHostConfig(config HostConfig) (err error) {
	_, err = c.makeRawRequest(HTTPPost, "/host", config.ToURLValues().Encode())

	return
}

// AddStorageFolder uses the /host/storage/folders/add api endpoint to
// add a storage folder to a host
func (c *Client) AddStorageFolder(path string, size uint64) (err error) {
	values := url.Values{}

	values.Set("path", path)
	values.Set("size", strconv.FormatUint(size, 10))

	_, err = c.makeRawRequest(HTTPPost, "/host/storage/folders/add", values.Encode())

	return
}

// RemoveStorageFolder uses the /host/storage/folders/remove api
// endpoint to remove a storage folder from a host.
func (c *Client) RemoveStorageFolder(path string) (err error) {
	values := url.Values{}

	values.Set("path", path)

	_, err = c.makeRawRequest(HTTPPost, "/host/storage/folders/remove", values.Encode())

	return
}

// ResizeStorageFolder uses the /host/storage/folders/resize api
// endpoint to resize an existing storage folder.
func (c *Client) ResizeStorageFolder(path string, size uint64) (err error) {
	values := url.Values{}

	values.Set("path", path)
	values.Set("newsize", strconv.FormatUint(size, 10))

	_, err = c.makeRawRequest(HTTPPost, "/host/storage/folders/resize", values.Encode())

	return
}

// GetHostStorage requests the /host/storage endpoint.
func (c *Client) GetHostStorage() (sg responses.StorageFolders, err error) {
	err = c.makeRequest(HTTPGet, "/host/storage", "", &sg)

	return
}

// DeleteStorageSector uses the /host/storage/sectors/delete endpoint
// to delete a sector from the host.
func (c *Client) DeleteStorageSector(root string) (err error) {
	_, err = c.makeRawRequest(HTTPPost, "/host/storage/sectors/delete/"+root, "")
	return
}
