package types

import (
	"database/sql/driver"
	"strconv"
	"time"
)

//Timestamp represents a Unix timestamp, i.e. the number of the seconds since Jan 1 1970.
type Timestamp struct {
	i time.Time
}

//MarshalJSON implements the json.Marshaler interface.
func (stamp Timestamp) MarshalJSON() ([]byte, error) {
	return []byte(strconv.FormatInt(stamp.i.Unix(), 10)), nil
}

//UnmarshalJSON implements the json.Unmarshaler interface. An error is returned if a negative number is provided.
func (stamp *Timestamp) UnmarshalJSON(data []byte) (err error) {
	secStr := string(data)

	seconds, err := strconv.ParseInt(secStr, 10, 64)

	if err != nil {
		return
	}

	stamp.i = time.Unix(seconds, 0)

	return
}

//Scan implements the sql.Scanner interface
func (stamp *Timestamp) Scan(src interface{}) error {
	return stamp.i.UnmarshalText(src.([]byte))
}

//Value implements the sql.Valuer interface
func (stamp Timestamp) Value() (val driver.Value, err error) {
	return stamp.i.String(), nil
}
