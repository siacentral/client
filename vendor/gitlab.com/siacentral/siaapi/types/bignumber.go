package types

import (
	"bytes"
	"database/sql/driver"
	"errors"
	"fmt"
	"math/big"
)

//BigNumber wraps big.Int with some additional helper functions
type BigNumber struct {
	i big.Int
}

//Big returns the big.Int version of this number
func (num *BigNumber) Big() *big.Int {
	return &num.i
}

//String prints the Big into a string
func (num *BigNumber) String() (val string) {
	return num.i.String()
}

//SetUint64 SetUint64
func (num *BigNumber) SetUint64(val uint64) *BigNumber {
	num.i.SetUint64(val)

	return num
}

//SetBig SetBig
func (num *BigNumber) SetBig(val *big.Int) *BigNumber {
	num.i.Set(val)

	return num
}

// Cmp compares two Big values. The return value follows the convention
// of math/big.
func (num *BigNumber) Cmp(y *BigNumber) int {
	return num.i.Cmp(&y.i)
}

// Cmp64 compares x to a uint64. The return value follows the convention of
// math/big.
func (num *BigNumber) Cmp64(y uint64) int {
	return num.Cmp(new(BigNumber).SetUint64(y))
}

// Equals returns true if x and y have the same value.
func (num *BigNumber) Equals(y *BigNumber) bool {
	return num.Cmp(y) == 0
}

// Equals64 returns true if x and y have the same value.
func (num *BigNumber) Equals64(y uint64) bool {
	return num.Cmp64(y) == 0
}

//Add sets num to the sum of num+un and returns num
func (num *BigNumber) Add(un *BigNumber) *BigNumber {
	num.i.Add(&num.i, &un.i)

	return num
}

//AddBig sets num to the sum of num+big and returns num
func (num *BigNumber) AddBig(big *big.Int) *BigNumber {
	num.i.Add(&num.i, big)

	return num
}

//Add64 sets num to the sum of num+y and returns num
func (num *BigNumber) Add64(y uint64) *BigNumber {
	num.i.Add(&num.i, new(big.Int).SetUint64(y))

	return num
}

//Sub sets num to the sum of num-un and returns num
func (num *BigNumber) Sub(un *BigNumber) *BigNumber {
	num.i.Sub(&num.i, &un.i)

	return num
}

//Sub64 sets num to the sum of num-y and returns num
func (num *BigNumber) Sub64(y uint64) *BigNumber {
	num.i.Sub(&num.i, new(big.Int).SetUint64(y))

	return num
}

// Mul sets num to the product num*y and returns num
func (num *BigNumber) Mul(y *BigNumber) *BigNumber {
	num.i.Mul(&num.i, &y.i)

	return num
}

// Mul64 sets num to the product num*y and returns num
func (num *BigNumber) Mul64(y uint64) *BigNumber {
	num.i.Mul(&num.i, new(big.Int).SetUint64(y))

	return num
}

// Div returns a new Big value c = x / y.
func (num *BigNumber) Div(y *BigNumber) *BigNumber {
	num.i.Div(&num.i, &y.i)

	return num
}

// Div64 sets num to the quotient num/y and returns num
func (num *BigNumber) Div64(y uint64) *BigNumber {
	num.i.Div(&num.i, new(big.Int).SetUint64(y))

	return num
}

//MarshalJSON implements the json.Marshaler interface.
func (num BigNumber) MarshalJSON() ([]byte, error) {
	return []byte(`"` + num.i.String() + `"`), nil
}

//UnmarshalJSON implements the json.Unmarshaler interface. An error is returned if a negative number is provided.
func (num *BigNumber) UnmarshalJSON(data []byte) (err error) {
	data = bytes.Trim(data, `"`)

	err = num.i.UnmarshalJSON(data)

	if err != nil {
		return
	}

	if num.i.Sign() < 0 {
		err = errors.New("no negative unit number")
		return
	}

	return
}

//Scan implements the sql.Scanner interface
func (num *BigNumber) Scan(src interface{}) error {
	source := src.([]byte)

	if _, success := num.i.SetString(string(source), 10); !success {
		return fmt.Errorf("error scanning value %s", string(source))
	}

	return nil
}

//Value implements the sql.Valuer interface
func (num BigNumber) Value() (val driver.Value, err error) {
	return num.i.String(), nil
}
