package responses

type (
	//Peer Peer
	Peer struct {
		Inbound    bool   `json:"inbound"`
		Local      bool   `json:"local"`
		NetAddress string `json:"netaddress"`
		Version    string `json:"version"`
	}

	//Gateway Gateway
	Gateway struct {
		NetAddress string `json:"netaddress"`
		Peers      []Peer `json:"peers"`
	}
)
