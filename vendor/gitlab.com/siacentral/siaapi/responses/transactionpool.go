package responses

import types "gitlab.com/siacentral/siaapi/types"

type (
	// TPoolFee contains the current estimated fee
	TPoolFee struct {
		Minimum types.BigNumber `json:"minimum"`
		Maximum types.BigNumber `json:"maximum"`
	}

	// TPoolRaw contains the requested transaction encoded to the raw
	// format, along with the id of that transaction.
	TPoolRaw struct {
		ID          string `json:"id"`
		Parents     []byte `json:"parents"`
		Transaction []byte `json:"transaction"`
	}

	// TPoolConfirmed contains information about whether or not
	// the transaction has been seen on the blockhain
	TPoolConfirmed struct {
		Confirmed bool `json:"confirmed"`
	}
)
