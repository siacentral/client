package siadaemon

import "sync"

const (
	//NotifyExit NotifyExit
	NotifyExit NotifierFlag = 1 << iota

	//NotifyCrash NotifyCrash
	NotifyCrash

	//NotifyShutdown NotifyShutdown
	NotifyShutdown

	//NotifyStart NotifyStart
	NotifyStart

	//NotifyLoad NotifyLoad
	NotifyLoad

	//NotifyStdOut NotifyStdOut
	NotifyStdOut

	//NotifyStdErr NotifyStdErr
	NotifyStdErr
)

var (
	notifiers   = make(map[chan<- Notifier]NotifierFlag)
	notifyMutex = sync.Mutex{}
)

type (
	//NotifierFlag NotifierFlag
	NotifierFlag uint8

	//Notifier Notifier
	Notifier struct {
		Message string
		PID     int
		Type    NotifierFlag
	}
)

//RegisterNotifier registers or updates the channel c to relay signals for the Sia daemon
func RegisterNotifier(c chan<- Notifier, args ...NotifierFlag) {
	notifyMutex.Lock()

	defer notifyMutex.Unlock()

	var flags NotifierFlag

	if len(args) == 0 {
		args = []NotifierFlag{NotifyExit}
	}

	for _, f := range args {
		flags = flags | f
	}

	notifiers[c] = flags
}

//UnregisterNotifier stops relaying the exit signal to the channel c
func UnregisterNotifier(c chan<- Notifier) {
	notifyMutex.Lock()

	defer notifyMutex.Unlock()

	delete(notifiers, c)
}

func notify(msg string, flag NotifierFlag) {
	notifyMutex.Lock()

	defer notifyMutex.Unlock()

	notifier := Notifier{
		Message: msg,
		PID:     PID(),
		Type:    flag,
	}

	for c, f := range notifiers {
		if f&flag != 0 {
			c <- notifier
		}
	}
}
