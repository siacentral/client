package siadaemon

import (
	"errors"
	"os"
	"os/exec"
	"sync"
	"time"
)

var (
	startTime time.Time
	siaDaemon *exec.Cmd

	sessionID string
	outputPid = -1

	output []string
	errOut []string
	stdOut []string

	siaConfig         SiadConfig
	siaFullyLoaded    bool
	firstLoadComplete bool
	exitMode          uint8

	outputMutex = sync.Mutex{}
)

const (
	exitModeUnknown uint8 = iota
	exitModeStop
	exitModeRestart
)

type (
	//SiadConfig holds all supported configuration variables for the Sia daemon
	SiadConfig struct {
		DataPath        string
		APIAgent        string
		HostPort        string
		APIAddr         string
		RPCPort         string
		Modules         string
		Path            string
		APIPassword     string
		WalletPassword  string
		StartTimeout    time.Duration
		ShutdownTimeout time.Duration
	}
)

func addStdOut(msg string, pid int) {
	outputMutex.Lock()

	defer outputMutex.Unlock()

	if outputPid != pid {
		output = []string{}
		stdOut = []string{}
		errOut = []string{}
		outputPid = pid
	}

	output = append(output, msg)
	stdOut = append(stdOut, msg)
}

func addErrOut(msg string, pid int) {
	outputMutex.Lock()

	defer outputMutex.Unlock()

	if outputPid != pid {
		output = []string{}
		stdOut = []string{}
		errOut = []string{}
		outputPid = pid
	}

	output = append(output, msg)
	errOut = append(errOut, msg)
}

//Loaded returns true if the sia process has fully loaded and is running
func Loaded() bool {
	return Running() && siaFullyLoaded
}

//SessionID returns the current unique session identifier for this sia daemon
func SessionID() string {
	return sessionID
}

//KillSia forcibly kills the sia process
func KillSia() error {
	return siaDaemon.Process.Kill()
}

//Running returns whether or not the sia daemon is currently running
func Running() bool {
	if siaDaemon == nil || siaDaemon.Process == nil {
		return false
	}

	return siaDaemon.ProcessState == nil
}

//PID returns the current process ID of the Sia Daemon
func PID() int {
	if siaDaemon == nil || siaDaemon.Process == nil {
		return -1
	}

	return siaDaemon.Process.Pid
}

//StdOut returns the stdout from Sia
func StdOut() ([]string, int) {
	return stdOut, outputPid
}

//StdErr returns the stderr from Sia
func StdErr() ([]string, int) {
	return errOut, outputPid
}

//Output returns all output from Sia
func Output() ([]string, []string, int) {
	return stdOut, errOut, outputPid
}

//StartTime gets the time that the sia process was started
func StartTime() time.Time {
	return startTime
}

func shutdown(mode uint8) {
	if !Running() {
		return
	}

	exitMode = mode
	notify("shutdown initiated", NotifyShutdown)

	if err := siaDaemon.Process.Signal(os.Interrupt); err != nil {
		siaDaemon.Process.Kill()
	}

	if _, err := blockUntilTimeout(siaConfig.ShutdownTimeout, NotifyExit); err != nil {
		siaDaemon.Process.Kill()
	}
}

//Restart shuts down and restarts Sia
func Restart() (err error) {
	shutdown(exitModeRestart)

	if _, err := blockUntilTimeout(siaConfig.ShutdownTimeout, NotifyStart); err != nil {
		err = errors.New("restart failed")
	}

	return
}

func startDaemon(config SiadConfig) (daemon *exec.Cmd, err error) {
	args := buildSiaArgs(config)
	daemon = exec.Command(config.Path, args...)

	if sessionID, err = randomKey(10); err != nil {
		return
	}

	exitMode = exitModeUnknown
	env := []string{}

	if len(config.APIPassword) > 0 {
		env = append(os.Environ(), "SIA_API_PASSWORD="+config.APIPassword)
	}

	if len(config.WalletPassword) > 0 {
		env = append(env, "SIA_WALLET_PASSWORD="+config.WalletPassword)
	}

	daemon.Env = env

	siaOut, err := daemon.StdoutPipe()

	if err != nil {
		return
	}

	siaErr, err := daemon.StderrPipe()

	if err != nil {
		return
	}

	go stderrReader(siaErr)
	go stdoutReader(siaOut)

	if err = daemon.Start(); err != nil {
		return
	}

	startTime = time.Now()
	notify("daemon started", NotifyStart)

	return
}

func watchDaemon(config SiadConfig) error {
	for {
		var msg string
		var err error

		siaDaemon, err = startDaemon(config)

		if err != nil {
			return err
		}

		if err := siaDaemon.Wait(); err != nil {
			msg = err.Error()
		}

		siaFullyLoaded = false

		switch exitMode {
		case exitModeStop:
			notify("sia daemon stopped", NotifyExit)
			return nil
		case exitModeUnknown:
			notify("sia daemon crashed. restarting", NotifyCrash)
		default:
			notify(msg, NotifyExit)
		}

		if !firstLoadComplete {
			return ErrFailedToLoad
		}
	}
}

//Stop stops the sia process and all monitoring
func Stop() {
	shutdown(exitModeStop)

	return
}

//Start starts and monitors the sia daemon process.
func Start(config SiadConfig) (err error) {
	siaConfig = config

	if Running() {
		err = ErrAlreadyRunning
		return
	}

	err = watchDaemon(config)

	return
}
