package methods

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"

	"gitlab.com/siacentral/client/config"
	"gitlab.com/siacentral/client/types"
	siaAPI "gitlab.com/siacentral/siaapi"
)

//GetStorageFolders gets all storage folders added to the Sia daemon
func GetStorageFolders() (resp types.StorageFoldersResponse) {
	resp.Type = types.MessageStorageListFolders

	siaClient := &siaAPI.Client{
		Address:   config.SiadAPIAddr,
		Password:  config.SiadAPIPassword,
		UserAgent: config.SiadAPIAgent,
	}

	storageInfo, err := siaClient.GetHostStorage()

	if err != nil {
		resp.Status = types.ResponseStatusErr
		resp.Message = "error getting siad storage info"

		return
	}

	for _, siaFolder := range storageInfo.Folders {
		var folder types.StorageFolder

		folder.Capacity = siaFolder.Capacity
		folder.RemainingCapacity = siaFolder.CapacityRemaining

		folder.FailedReads = siaFolder.FailedReads
		folder.FailedWrites = siaFolder.FailedWrites

		folder.SuccessfulReads = siaFolder.SuccessfulReads
		folder.SuccessfulWrites = siaFolder.SuccessfulWrites
		folder.Path = siaFolder.Path

		resp.FolderCount++

		resp.Capacity.Add(&siaFolder.Capacity)
		resp.RemainingCapacity.Add(&siaFolder.CapacityRemaining)

		resp.Folders = append(resp.Folders, folder)
	}

	resp.Status = types.ResponseStatusSuccess

	return
}

//AddStorageFolder adds a new storage folder to the Sia daemon
func AddStorageFolder(request types.FolderRequest) (resp types.StorageFoldersResponse) {
	var err error

	resp.Type = types.MessageStorageAddFolder

	defer func() {
		if err != nil {
			resp.Status = types.ResponseStatusErr
			resp.Message = err.Error()

			return
		}

		resp.Status = types.ResponseStatusSuccess
	}()

	path := filepath.Clean(request.Path)

	if _, err = os.Stat(path); err == nil {
		err = fmt.Errorf("folder %s already exists", path)
		return
	}

	if err = os.MkdirAll(path, 0755); err != nil {
		return
	}

	siaClient := &siaAPI.Client{
		Address:   config.SiadAPIAddr,
		Password:  config.SiadAPIPassword,
		UserAgent: config.SiadAPIAgent,
	}

	if err = siaClient.AddStorageFolder(path, request.Size); err != nil {
		return
	}

	resp = GetStorageFolders()
	resp.Type = types.MessageStorageAddFolder

	for _, folder := range resp.Folders {
		if folder.Path != request.Path {
			continue
		}

		resp.ChangedFolder = folder
	}

	return
}

//ResizeStorageFolder resizes an existing storage folder on the Sia daemon
func ResizeStorageFolder(request types.FolderRequest) (resp types.StorageFoldersResponse) {
	var err error

	resp.Type = types.MessageStorageResizeFolder

	defer func() {
		if err != nil {
			resp.Status = types.ResponseStatusErr
			resp.Message = err.Error()

			return
		}

		resp.Status = types.ResponseStatusSuccess
	}()

	if _, err := os.Stat(request.Path); err != nil {
		return
	}

	siaClient := &siaAPI.Client{
		Address:   config.SiadAPIAddr,
		Password:  config.SiadAPIPassword,
		UserAgent: config.SiadAPIAgent,
	}

	if err := siaClient.ResizeStorageFolder(request.Path, request.Size); err != nil {
		return
	}

	resp = GetStorageFolders()
	resp.Type = types.MessageStorageResizeFolder

	if resp.Status != types.ResponseStatusSuccess {
		err = errors.New(resp.Message)
		return
	}

	for _, folder := range resp.Folders {
		if folder.Path != request.Path {
			continue
		}

		resp.ChangedFolder = folder
	}

	return
}

//RemoveStorageFolder removes an existing storage folder from the Sia daemon
func RemoveStorageFolder(request types.FolderRequest) (resp types.StorageFoldersResponse) {
	var err error

	resp.Type = types.MessageStorageRemoveFolder

	defer func() {
		if err != nil {
			resp.Status = types.ResponseStatusErr
			resp.Message = err.Error()

			return
		}

		resp.Status = types.ResponseStatusSuccess
	}()

	if _, err := os.Stat(request.Path); err != nil {
		return
	}

	siaClient := &siaAPI.Client{
		Address:   config.SiadAPIAddr,
		Password:  config.SiadAPIPassword,
		UserAgent: config.SiadAPIAgent,
	}

	if err := siaClient.RemoveStorageFolder(request.Path); err != nil {
		return
	}

	resp = GetStorageFolders()
	resp.Type = types.MessageStorageRemoveFolder
	resp.ChangedFolder = types.StorageFolder{
		Path: request.Path,
	}

	return
}
