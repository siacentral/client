package methods

import (
	"gitlab.com/siacentral/client/types"
	"gitlab.com/siacentral/client/config"
	siaAPI "gitlab.com/siacentral/siaapi"
	"gitlab.com/siacentral/siadaemon"
)

//GetDaemonStatus GetDaemonStatus
func GetDaemonStatus() (resp types.DaemonStatusResponse) {
	resp.Type = types.MessageDaemonStatus

	stdout, errout, pid := siadaemon.Output()

	resp.Output = stdout
	resp.Errors = errout
	resp.PID = pid
	resp.SessionID = siadaemon.SessionID()
	resp.Running = siadaemon.Running()
	resp.StartTime = siadaemon.StartTime()

	if siadaemon.Loaded() {
		resp.DaemonStatus = types.DaemonStatusLoaded
	} else if siadaemon.Running() {
		resp.DaemonStatus = types.DaemonStatusStarted
	} else {
		resp.DaemonStatus = types.DaemonStatusStopped
	}

	resp.Status = types.ResponseStatusSuccess

	return
}

//UpdateSiaDaemon triggers an update on the Sia daemon if necessary
func UpdateSiaDaemon() (resp types.ResponseBase) {
	resp.Type = types.MessageDaemonUpdate

	siaClient := &siaAPI.Client{
		Address:   config.SiadAPIAddr,
		Password:  config.SiadAPIPassword,
		UserAgent: config.SiadAPIAgent,
	}

	if err := siaClient.UpdateDaemon(); err != nil {
		resp.Status = types.ResponseStatusErr
		resp.Message = err.Error()

		return
	}

	resp.Status = types.ResponseStatusSuccess
	resp.Message = "updated sia daemon"

	return
}
