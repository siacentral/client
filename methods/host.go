package methods

import (
	"gitlab.com/siacentral/client/types"
	"gitlab.com/siacentral/client/config"
	siaAPI "gitlab.com/siacentral/siaapi"
)

//GetHostSettings gets the current hosts configuration from Sia
func GetHostSettings() (resp types.HostConfigResponse) {
	resp.Type = types.MessageHostConfig

	siaClient := &siaAPI.Client{
		Address:   config.SiadAPIAddr,
		Password:  config.SiadAPIPassword,
		UserAgent: config.SiadAPIAgent,
	}

	host, err := siaClient.GetHostStats()

	if err != nil {
		resp.Status = types.ResponseStatusErr
		resp.Message = "error getting active hosts"
		return
	}

	resp.AcceptingContracts = host.InternalSettings.AcceptingContracts

	resp.MaxDownloadBatchSize = host.InternalSettings.MaxDownloadBatchSize
	resp.MaxDuration = host.InternalSettings.MaxDuration
	resp.MaxReviseBatchSize = host.InternalSettings.MaxReviseBatchSize
	resp.WindowSize = host.InternalSettings.WindowSize

	resp.NetAddress = host.InternalSettings.NetAddress

	resp.Collateral = host.InternalSettings.Collateral
	resp.CollateralBudget = host.InternalSettings.CollateralBudget
	resp.MaxCollateral = host.InternalSettings.MaxCollateral
	resp.MinContractPrice = host.InternalSettings.MinContractPrice
	resp.MinDownloadPrice = host.InternalSettings.MinDownloadBandwidthPrice
	resp.MinStoragePrice = host.InternalSettings.MinStoragePrice
	resp.MinUploadPrice = host.InternalSettings.MinUploadBandwidthPrice
	resp.Status = types.ResponseStatusSuccess

	return
}

//UpdateHostConfig updates the host's configuration in Sia
func UpdateHostConfig(request types.UpdateHostConfigRequest) (resp types.ResponseBase) {
	resp.Type = types.MessageHostUpdateConfig

	siaClient := &siaAPI.Client{
		Address:   config.SiadAPIAddr,
		Password:  config.SiadAPIPassword,
		UserAgent: config.SiadAPIAgent,
	}

	config := siaAPI.HostConfig{
		siaAPI.HostParamCollateralBudget:          request.CollateralBudget.String(),
		siaAPI.HostParamMaxCollateral:             request.MaxCollateral.String(),
		siaAPI.HostParamMinContractPrice:          request.MinContractPrice.String(),
		siaAPI.HostParamMinDownloadBandwidthPrice: request.MinDownloadPrice.String(),
		siaAPI.HostParamMinUploadBandwidthPrice:   request.MinUploadPrice.String(),
		siaAPI.HostParamCollateral:                request.Collateral.String(),
		siaAPI.HostParamMinStoragePrice:           request.MinStoragePrice.String(),
		siaAPI.HostParamAcceptingContracts:        request.AcceptingContracts,
		siaAPI.HostParamMaxDuration:               request.MaxDuration,
		siaAPI.HostParamWindowSize:                request.WindowSize,
		siaAPI.HostParamMaxDownloadBatchSize:      request.MaxDownloadBatchSize,
		siaAPI.HostParamMaxReviseBatchSize:        request.MaxReviseBatchSize,
		siaAPI.HostParamNetAddress:                request.NetAddress,
	}

	err := siaClient.UpdateHostConfig(config)

	if err != nil {
		resp.Status = types.ResponseStatusErr
		resp.Message = err.Error()
		return
	}

	resp.Status = types.ResponseStatusSuccess

	return
}

//GetHostStatus get the current status of the host daemon
func GetHostStatus() (resp types.HostStatusResponse) {
	var err error

	resp.Type = types.MessageHostStatus

	defer func() {
		if err != nil {
			resp.Status = types.ResponseStatusErr
			resp.Message = err.Error()
		}

		resp.Status = types.ResponseStatusSuccess
	}()

	siaClient := &siaAPI.Client{
		Address:   config.SiadAPIAddr,
		Password:  config.SiadAPIPassword,
		UserAgent: config.SiadAPIAgent,
	}

	consensusInfo, err := siaClient.GetConsensus()

	if err != nil {
		return
	}

	version, err := siaClient.GetDaemonVersion()

	if err != nil {
		return
	}

	hostInfo, err := siaClient.GetHostStats()

	if err != nil {
		return
	}

	semver, err := types.ParseSemVer(version.Version)

	if err != nil {
		return
	}

	resp.BlockHeight = uint64(consensusInfo.Height)
	resp.Version = semver
	resp.PublicKey = config.HostPublicKey
	resp.NetAddress = hostInfo.ExternalSettings.NetAddress
	resp.Synced = consensusInfo.Synced

	return
}

//AnnounceHost announces the sia host to the network
func AnnounceHost(addr string) (resp types.ResponseBase) {
	resp.Type = types.MessageHostAnnounce

	siaClient := &siaAPI.Client{
		Address:   config.SiadAPIAddr,
		Password:  config.SiadAPIPassword,
		UserAgent: config.SiadAPIAgent,
	}

	var err error

	if len(addr) > 0 {
		err = siaClient.AnnounceHostAddr(addr)
	} else {
		err = siaClient.AnnounceHost()
	}

	if err != nil {
		resp.Status = types.ResponseStatusErr
		resp.Message = err.Error()

		return
	}

	resp.Status = types.ResponseStatusSuccess

	return
}
