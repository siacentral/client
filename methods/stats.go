package methods

import (
	"errors"
	"strings"

	"gitlab.com/siacentral/client/config"
	"gitlab.com/siacentral/client/consts"
	"gitlab.com/siacentral/client/system/netusage"
	"gitlab.com/siacentral/client/types"

	siaAPI "gitlab.com/siacentral/siaapi"
)

var lastRPCUsage, lastHostUsage netusage.PortUsageStat

//GetHostMetrics gets basic statistics and usage information from the Sia daemon
func GetHostMetrics() (resp types.HostMetricsResponse) {
	var err error

	resp.Type = types.MessageHostMetrics

	defer func() {
		if err != nil {
			resp.Status = types.ResponseStatusErr
			resp.Message = err.Error()
		}

		resp.Status = types.ResponseStatusSuccess
	}()

	siaClient := &siaAPI.Client{
		Address:   config.SiadAPIAddr,
		Password:  config.SiadAPIPassword,
		UserAgent: config.SiadAPIAgent,
	}

	hostInfo, err := siaClient.GetHostStats()

	if err != nil {
		return
	}

	var rpcPort, hostPort string

	rpcPortSlice := strings.Split(config.SiadRPCPort, ":")
	hostPortSlice := strings.Split(config.SiadHostPort, ":")

	if len(rpcPortSlice) > 1 {
		rpcPort = rpcPortSlice[1]
	} else {
		rpcPort = rpcPortSlice[0]
	}

	if len(hostPortSlice) > 1 {
		hostPort = hostPortSlice[1]
	} else {
		hostPort = hostPortSlice[0]
	}

	rpcPort = strings.TrimSpace(rpcPort)
	hostPort = strings.TrimSpace(hostPort)

	resp.ClientVersion = consts.Version
	resp.NetAddress = hostInfo.ExternalSettings.NetAddress
	resp.PublicKey = config.HostPublicKey
	resp.ContractCount = hostInfo.FinancialMetrics.ContractCount
	resp.WorkingStatus = string(hostInfo.WorkingStatus)
	resp.HostPort = hostPort

	networkUsage := netusage.GetPortUsageStats()

	rpcUsage := networkUsage[rpcPort]
	hostUsage := networkUsage[hostPort]

	defer func() {
		if err != nil {
			return
		}

		lastRPCUsage = rpcUsage
		lastHostUsage = hostUsage
	}()

	resp.RPCSent.SetUint64(rpcUsage.Sent - lastRPCUsage.Sent)
	resp.RPCRecv.SetUint64(rpcUsage.Recv - lastRPCUsage.Recv)
	resp.HostSent.SetUint64(hostUsage.Sent - lastHostUsage.Sent)
	resp.HostRecv.SetUint64(hostUsage.Recv - lastHostUsage.Recv)

	walletInfo, err := siaClient.GetWallet()

	if err != nil {
		return
	}

	consensusInfo, err := siaClient.GetConsensus()

	if err != nil {
		return
	}

	version, err := siaClient.GetDaemonVersion()

	if err != nil {
		return
	}

	semver, err := types.ParseSemVer(version.Version)

	if err != nil {
		return
	}

	resp.SiaVersion = semver
	resp.LastBlock = consensusInfo.CurrentBlock
	resp.BlockHeight = uint64(consensusInfo.Height)

	resp.WalletUnlocked = walletInfo.Unlocked
	resp.ConfirmedBalance.SetBig(walletInfo.ConfirmedSiacoinBalance.Big())
	resp.StoragePrice.SetBig(hostInfo.ExternalSettings.StoragePrice.Big())
	resp.ContractCompensation.SetBig(hostInfo.FinancialMetrics.ContractCompensation.Big())
	resp.MaxCollateral.SetBig(hostInfo.InternalSettings.CollateralBudget.Big())
	resp.RiskedCollateral.SetBig(hostInfo.FinancialMetrics.RiskedStorageCollateral.Big())
	resp.LockedCollateral.SetBig(hostInfo.FinancialMetrics.LockedStorageCollateral.Big())
	resp.Difficulty.SetBig(consensusInfo.Difficulty.Big())

	resp.Synced = consensusInfo.Synced
	resp.AcceptingContracts = hostInfo.ExternalSettings.AcceptingContracts
	storageInfo, err := siaClient.GetHostStorage()

	if err != nil {
		return
	}

	for _, folder := range storageInfo.Folders {
		resp.Capacity.Add(&folder.Capacity)
		resp.RemainingCapacity.Add(&folder.CapacityRemaining)
	}

	contractResp := GetHostContracts()

	if contractResp.Status != types.ResponseStatusSuccess {
		err = errors.New(contractResp.Message)
		return
	}

	resp.Contracts = contractResp.Contracts
	resp.Status = types.ResponseStatusSuccess

	return
}

//GetHostContracts gets all storage obligations managed by the Sia daemon
func GetHostContracts() (resp types.ContractsResponse) {
	resp.Type = types.MessageDataListContracts

	siaClient := &siaAPI.Client{
		Address:   config.SiadAPIAddr,
		Password:  config.SiadAPIPassword,
		UserAgent: config.SiadAPIAgent,
	}

	contracts, err := siaClient.GetHostContracts()

	if err != nil {
		resp.Status = types.ResponseStatusErr
		resp.Message = "error getting storage obligation info"
		return
	}

	for _, obligation := range contracts.Contracts {
		var contract types.SiaContract

		contract.ObligationID = obligation.ObligationID
		contract.ObligationStatus = obligation.ObligationStatus
		contract.SectorRootsCount = obligation.SectorRootsCount

		contract.ContractCost = obligation.ContractCost
		contract.LockedCollateral = obligation.LockedCollateral
		contract.PotentialDownloadRevenue = obligation.PotentialDownloadRevenue
		contract.PotentialStorageRevenue = obligation.PotentialStorageRevenue
		contract.PotentialUploadRevenue = obligation.PotentialUploadRevenue
		contract.RiskedCollateral = obligation.RiskedCollateral
		contract.TransactionFeesAdded = obligation.TransactionFeesAdded

		contract.DataSize.SetUint64(obligation.DataSize)
		contract.ExpirationHeight.SetUint64(obligation.ExpirationHeight)
		contract.NegotiationHeight.SetUint64(obligation.NegotiationHeight)
		contract.ProofDeadline.SetUint64(obligation.ProofDeadLine)

		contract.OriginConfirmed = obligation.OriginConfirmed
		contract.ProofConfirmed = obligation.ProofConfirmed
		contract.ProofConstructed = obligation.ProofConstructed
		contract.RevisionConfirmed = obligation.RevisionConfirmed
		contract.RevisionConstructed = obligation.RevisionConstructed

		resp.Contracts = append(resp.Contracts, contract)
	}

	resp.Status = types.ResponseStatusSuccess

	return
}
