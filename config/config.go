package config

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"io/ioutil"
)

var (
	//ErrInvalidKey occurs when the key specified in the config is not in the proper format
	ErrInvalidKey = errors.New("invalid key format")
)

var (
	//HostFingerprint the sha256 checksum of the public_key
	HostFingerprint string
	//CommandPublicKey the key used to verify commands came from the user
	CommandPublicKey *[32]byte
	//ServerPublicKey the key used to sign responses sent back to the server
	ServerPublicKey *[32]byte
	//ClientPrivateKey the private key used to sign and verify encrypted messages
	ClientPrivateKey *[32]byte
	//ClientPublicKey the public key used to sign and verify encrypted messages
	ClientPublicKey *[32]byte
	//HostPublicKey the public key of the host
	HostPublicKey string
	//ServerAddr the url of the central server to connect to
	ServerAddr = "app.siacentral.com:10002"
	//CertFingerprint the base64 encoded sha256 checksum of the server's TLS certificate for certificate pinning
	CertFingerprint = ""
	//SiadPath the file path to the siad binary to run the Sia daemon
	SiadPath string
	//SiadAPIPassword the password that siad should use for its internal api. sets the SIA_API_PASSWORD environment variable temporarily.
	SiadAPIPassword string
	//SiadWalletPassword the password that siad should use to automatically unlock the wallet. Sets the SIA_WALLET_PASSWORD environment variable temporarily. Not necessary if your wallet is already set up for auto unlock
	SiadWalletPassword string
	//SiadAPIAgent the api agent that siad should use. Corresponds to "--agent" option
	SiadAPIAgent = "Sia-Agent"
	//SiadAPIAddr the url that siad listens on. Corresponds to "--api-addr" option
	SiadAPIAddr = "localhost:9980"
	//SiadDataPath the data path that siad should use. Corresponds to "--sia-directory" option
	SiadDataPath string
	//SiadHostPort the host port that siad should listen on. Cooresponds to "--host-addr" option
	SiadHostPort = ":9982"
	//SiadRPCPort the rpc port that siad should listen on. Corresponds to the "--rpc-addr" option
	SiadRPCPort = ":9981"
	//SiadModules the modules that siad should load. Corresponds to the "-m" option
	SiadModules = "gctrwh"
	//DisableCertVerification allows the tcp connection to connect without verifying the server certificate. Insecure option, should be used for testing only.
	DisableCertVerification = false
	//EnableCertPin allows the tcp connection to connect only to a server with the specified CertFingerprint. Provides extra security when connecting to a server.
	EnableCertPin = false
)

type (
	//baseConfig contains all configuration for the client instance
	baseConfig struct {
		PublicKey               string `json:"public_key"`
		SecretKey               string `json:"secret_key"`
		ServerAddr              string `json:"server_addr"`
		CertFingerprint         string `json:"certificate_fingerprint"`
		SiadPath                string `json:"siad_path"`
		SiadAPIPassword         string `json:"siad_api_password"`
		SiadWalletPassword      string `json:"siad_wallet_password"`
		SiadAPIAgent            string `json:"siad_api_agent"`
		SiadAPIAddr             string `json:"siad_api_addr"`
		SiadDataPath            string `json:"siad_data_path"`
		SiadHostPort            string `json:"siad_host_port"`
		SiadRPCPort             string `json:"siad_rpc_port"`
		SiadModules             string `json:"siad_modules"`
		DisableCertVerification bool   `json:"disable_cert_verification"`
		EnableCertPin           bool   `json:"enable_cert_pinning"`
		DisableTLS              bool   `json:"disable_tls"`
	}
)

//ParseConfig parses the config json at the specified path and updates the global variables
func ParseConfig(configPath string) (err error) {
	var config baseConfig
	var clientPrivKey, clientPubKey, commandPubKey, serverPubKey [32]byte

	configFile, err := ioutil.ReadFile(configPath)

	if err != nil {
		return
	}

	if err = json.Unmarshal(configFile, &config); err != nil {
		return
	}

	keys, err := base64.StdEncoding.DecodeString(config.PublicKey)

	if err != nil {
		return
	}

	if len(keys) != 64 {
		err = ErrInvalidKey
		return
	}

	copy(serverPubKey[:], keys[0:32])
	copy(commandPubKey[:], keys[32:64])

	keys, err = base64.StdEncoding.DecodeString(config.SecretKey)

	if err != nil {
		return
	}

	if len(keys) != 64 {
		err = ErrInvalidKey
		return
	}

	copy(clientPrivKey[:], keys[:32])
	copy(clientPubKey[:], keys[32:64])

	ClientPrivateKey = &clientPrivKey
	ClientPublicKey = &clientPubKey
	ServerPublicKey = &serverPubKey
	CommandPublicKey = &commandPubKey

	if len(config.ServerAddr) > 0 {
		ServerAddr = config.ServerAddr
	}

	if len(config.CertFingerprint) > 0 {
		CertFingerprint = config.CertFingerprint
	}

	SiadPath = config.SiadPath
	SiadWalletPassword = config.SiadWalletPassword

	if len(config.SiadAPIAgent) > 0 {
		SiadAPIAgent = config.SiadAPIAgent
	}

	if len(config.SiadAPIAddr) > 0 {
		SiadAPIAddr = config.SiadAPIAddr
	}

	SiadDataPath = config.SiadDataPath

	if len(config.SiadHostPort) > 0 {
		SiadHostPort = config.SiadHostPort
	}

	if len(config.SiadRPCPort) > 0 {
		SiadRPCPort = config.SiadRPCPort
	}

	if len(config.SiadModules) > 0 {
		SiadModules = config.SiadModules
	}

	DisableCertVerification = config.DisableCertVerification
	EnableCertPin = config.EnableCertPin

	return
}
