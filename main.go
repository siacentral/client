package main

import (
	"errors"
	"io/ioutil"
	"log"
	"os"
	"os/signal"
	"path/filepath"
	"strings"
	"time"

	"gitlab.com/siacentral/client/methods"
	"gitlab.com/siacentral/client/types"
	"gitlab.com/siacentral/client/utils"

	"gitlab.com/siacentral/client/config"
	"gitlab.com/siacentral/client/connection"
	"gitlab.com/siacentral/client/handlers"
	"gitlab.com/siacentral/client/system/netusage"
	"gitlab.com/siacentral/siadaemon"
)

var (
	shuttingdown = false
	hasLoaded    = false
	errchan      = make(chan error, 1)
)

func startTCP() {
	connected := make(chan bool, 1)
	log.Println("Connecting to SiaCentral")
	handlers.AddConnectionHandlers()

	connection.RegisterConnectionNotifier(connected)

	go func() {
		for {
			err := connection.Connect()

			if err != nil {
				log.Println(err)
			}

			if shuttingdown {
				return
			}

			log.Println("Disconnected from SiaCentral")
			time.Sleep(time.Second * 30)
			log.Println("Reconnecting to SiaCentral")
		}
	}()

	<-connected

	connection.UnregisterConnectionNotifier(connected)
}

func startNetworkUsageMonitor() {
	err := netusage.StartCapture()

	if err != nil {
		log.Println("Network monitoring disabled - unable to initialize monitoring on the network interface")
		log.Fatalln(err)
	}
}

func startDaemon() {
	if err := siadaemon.Start(siadaemon.SiadConfig{
		Path:            config.SiadPath,
		DataPath:        config.SiadDataPath,
		HostPort:        config.SiadHostPort,
		RPCPort:         config.SiadRPCPort,
		APIAgent:        config.SiadAPIAgent,
		APIAddr:         config.SiadAPIAddr,
		Modules:         config.SiadModules,
		APIPassword:     config.SiadAPIPassword,
		WalletPassword:  config.SiadWalletPassword,
		StartTimeout:    time.Hour * 96,
		ShutdownTimeout: time.Minute * 10,
	}); err != nil {
		errchan <- err
		return
	}
}

func handleDaemonUpdates() {
	notifier := make(chan siadaemon.Notifier, 1)

	defer siadaemon.UnregisterNotifier(notifier)

	siadaemon.RegisterNotifier(notifier,
		siadaemon.NotifyCrash,
		siadaemon.NotifyLoad,
		siadaemon.NotifyShutdown,
		siadaemon.NotifyStdErr,
		siadaemon.NotifyStdOut)

	for msg := range notifier {
		resp := methods.GetDaemonStatus()

		switch msg.Type {
		case siadaemon.NotifyCrash: //triggers a notification using a different message type
			if !hasLoaded { //we don't want to spam errors if Sia has never loaded before
				return
			}

			resp.Type = types.MessageDaemonState
			resp.DaemonStatus = types.DaemonStatusCrashed
		case siadaemon.NotifyExit: //triggers a notification using a different message type
			if !hasLoaded { //we don't want to spam errors if Sia has never loaded before
				return
			}

			resp.Type = types.MessageDaemonState
			resp.DaemonStatus = types.DaemonStatusRestart
		case siadaemon.NotifyLoad: //triggers a notification using a different message type
			resp.Type = types.MessageDaemonState
			resp.DaemonStatus = types.DaemonStatusLoaded
			hasLoaded = true

			handlers.SyncHostStats()
		case siadaemon.NotifyStdOut:
			resp.Type = types.MessageDaemonStatus
			log.Println("siad:", msg.Message)
		case siadaemon.NotifyStdErr:
			resp.Type = types.MessageDaemonError
			log.Println("siad:", msg.Message)
		}

		if err := connection.SendMessage(&resp); err != nil {
			log.Println(err)
		}
	}
}

func startSiaDaemon() {
	log.Println("Starting Sia daemon")

	go handleDaemonUpdates()
	go startDaemon()
}

func syncStats() {
	exitNotifier := make(chan siadaemon.Notifier, 1)

	siadaemon.RegisterNotifier(exitNotifier, siadaemon.NotifyExit, siadaemon.NotifyShutdown)

	defer siadaemon.UnregisterNotifier(exitNotifier)

	for {
		syncTime := time.Until(utils.GetNearestTime(time.Now(), time.Minute*5))

		log.Printf("Syncing host statistics in %s", syncTime)

		select {
		case <-time.After(syncTime):
			handlers.SyncHostStats()
		case <-exitNotifier:
			log.Println("Sia Daemon shutting down, stopping stat sync")
			return
		}
	}
}

func startHostStatSync() {
	go func() {
		if !siadaemon.Loaded() {
			loadNotifier := make(chan siadaemon.Notifier, 1)

			siadaemon.RegisterNotifier(loadNotifier, siadaemon.NotifyLoad)

			<-loadNotifier

			siadaemon.UnregisterNotifier(loadNotifier)
		}

		loadSiadDefaults()

		for {
			syncStats()
		}
	}()
}

//checkOrDownloadSiad if siad already exists at the specified path or current working directory returns the path otherwise downloads the latest sia release and returns the downloaded path
func checkOrDownloadSiad(pwd string) (path string, err error) {
	var stat os.FileInfo

	path = config.SiadPath

	if len(path) > 0 {
		//check if the explicitly defined siad_path exists and error if it doesn't
		if stat, err = os.Stat(path); err != nil || (err == nil && stat.IsDir()) {
			err = errors.New("invalid siad_path")
			return
		}

		return
	}

	path = filepath.Join(pwd, utils.GetExecName("siad"))

	//check if siad exists in the current directory
	if stat, err = os.Stat(path); err == nil && !stat.IsDir() {
		return
	}

	log.Println("Sia Daemon is not installed. Downloading Latest Release.")

	return utils.DownloadSiaDaemon(pwd)
}

func loadSiadDefaults() {
	if len(config.SiadAPIPassword) == 0 {
		passBuf, err := ioutil.ReadFile(filepath.Join(utils.DefaultSiaDir(), "apipassword"))

		if err != nil {
			errchan <- errors.New("unable to read default api password and no password has been specified")
			return
		}

		config.SiadAPIPassword = strings.TrimSpace(string(passBuf))
	}

	publicKeyPath := config.SiadDataPath

	if len(config.SiadDataPath) == 0 {
		publicKeyPath = filepath.Dir(config.SiadPath)
	}

	publicKeyPath = filepath.Join(publicKeyPath, "host", "host.json")

	if key, err := utils.GetHostPublicKey(publicKeyPath); err == nil {
		config.HostPublicKey = key
	} else {
		errchan <- errors.New("unable to get host public key make sure siad_data_path is correct")
	}
}

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	log.Println("Parsing config")

	ex, err := os.Executable()

	if err != nil {
		log.Fatalln(err)
	}

	pwd := filepath.Dir(ex)
	configPath := filepath.Join(pwd, "config.json")

	if _, err := os.Stat(configPath); err != nil {
		configPath = "config.json"
	}

	if err := config.ParseConfig(configPath); err != nil {
		log.Fatalln(err)
	}

	installPath, err := checkOrDownloadSiad(pwd)

	if err != nil {
		log.Fatalln(err)
	}

	config.SiadPath = installPath

	startNetworkUsageMonitor()
	startTCP()
	startHostStatSync()
	startSiaDaemon()

	c := make(chan os.Signal, 1)

	signal.Notify(c, os.Interrupt, os.Kill)

	select {
	case sig := <-c:
		log.Printf("Caught %s signal, shutting down", sig)
	case err := <-errchan:
		log.Println(err)
	}

	shuttingdown = true

	connection.Close()
	siadaemon.Stop()
}
