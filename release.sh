#!/bin/bash

PRIVKEY=$1

# create fresh dist folder
rm -rf dist
mkdir dist

# generate public key from private key
openssl rsa -in ${PRIVKEY} -outform PEM -pubout -out dist/siacentral.pub.pem

set -e

for os in darwin linux windows; do
	platforms=( amd64 )

	# add the arm arch for linux
	if [ "$os" == "linux" ]; then
		platforms=( amd64 arm )
	fi

	for arch in ${platforms[@]}; do
		bin=siacentral

		# different naming convention for windows
		if [ "$os" == "windows" ]; then
			bin=siacentral.exe
		fi

		rm -f dist/$bin

		GOOS=${os} GOARCH=${arch} go build -ldflags "-extldflags '-static'" -o dist/$bin main.go
		openssl dgst -sha256 -sign ${PRIVKEY} -out dist/$bin.sha256 dist/$bin

		cd dist
		name="siacentral-${os}-${arch}.zip"

		zip -m $name $bin $bin.sha256
		cd ..
	done
done
