package connection

import (
	"bufio"
	"crypto/tls"
	"crypto/x509"
	"encoding/binary"
	"encoding/json"
	"errors"
	"io"
	"log"
	"net"
	"reflect"
	"strings"
	"sync"
	"time"

	"gitlab.com/siacentral/client/config"
	"gitlab.com/siacentral/client/consts"
	"gitlab.com/siacentral/client/crypto"
	"gitlab.com/siacentral/client/types"
)

var (
	conn *tls.Conn

	handlers            = make(map[string]ConnHandlerFunc)
	receive             = make(chan []byte, 1)
	errChan             = make(chan error, 1)
	closing             = make(chan bool, 1)
	connectionNotifiers = make(map[chan bool]interface{})
	notifierLock        = sync.Mutex{}
	shutdown            = false

	//ErrPinnedCertNotFound indicates that the pinned certificate was not found during verification
	ErrPinnedCertNotFound = errors.New("pinned certificate not found")
	//ErrNotConnected indicates that the client attempted to send a message but was not connected
	ErrNotConnected = errors.New("not connected to siacentral")
	//ErrRequestSize ErrRequestSize
	ErrRequestSize = errors.New("message content too large")

	//MessageTypeHandshake denotes a handshake message
	MessageTypeHandshake = MessageType(0)
	//MessageTypeInsecure denote an insecure unsigned message
	MessageTypeInsecure = MessageType(1)
	//MessageTypeInfo denotes a message signed by the SiaCentral server for information
	MessageTypeInfo = MessageType(2)
	//MessageTypeError denotes a message signed by the SiaCentral server for error
	MessageTypeError = MessageType(3)
	//MessageTypeCommand denotes a message signed by the user to issue a command
	MessageTypeCommand = MessageType(4)

	protocolVersion = UInt16ToByte(1)
)

const (
	Byte ByteSize = 1 << (10 * iota)
	Kilobyte
	Megabyte
	Gigabyte
	Terabyte
	Exabyte
)

type (
	//ConnHandlerFunc handles commands sent to the client
	ConnHandlerFunc func(data map[string]interface{}) interface{}

	//MessageType MessageType
	MessageType uint16

	ByteSize uint64
)

//handleMessages handles messages received, sending heartbeats, and reconnecting to the server when disconnected
func handleMessages() {
	for {
		select {
		case data := <-receive:
			messageType := MessageType(binary.LittleEndian.Uint16(data[2:4]))

			go parseMessage(data[4:], messageType)
		case <-time.After(15 * time.Second):
			if err := write([]byte("ping"), MessageTypeInsecure); err != nil {
				errChan <- err
			}
		case <-closing:
			return
		}
	}
}

func parseCommandMessage(data []byte) (err error) {
	var tmp map[string]interface{}
	var requestType string
	var requestID string

	defer func() {
		if err != nil {
			log.Println(err)
		}
	}()

	if err = crypto.ParseMessage(data, config.CommandPublicKey, config.ClientPrivateKey, &tmp); err != nil {
		return
	}

	if typeInterface, exists := tmp["message_type"]; exists {
		if typeStr, converted := typeInterface.(string); converted {
			requestType = strings.ToLower(typeStr)
		}
	}

	if len(requestType) == 0 {
		return
	}

	if idInterface, exists := tmp["id"]; exists {
		if idStr, converted := idInterface.(string); converted {
			requestID = idStr
		}
	}

	if handler, exists := handlers[requestType]; exists {
		var resp interface{}

		resp = handler(tmp)

		//globally sets the response ID, Type and Version
		respType := reflect.ValueOf(resp)
		respPointer := reflect.New(respType.Type()).Elem()

		respPointer.Set(respType)

		if typeField := respPointer.FieldByName("Type"); typeField.IsValid() {
			typeField.SetString(requestType)
		}

		if idField := respPointer.FieldByName("ID"); idField.IsValid() {
			idField.SetString(requestID)
		}

		if versionField := respPointer.FieldByName("ClientVersion"); versionField.IsValid() {
			versionField.Set(reflect.ValueOf(consts.Version))
		}

		err = SendMessage(respPointer.Interface())
	} else {
		log.Printf("Unknown request received - %s", requestType)
	}

	return
}

func parseInfoMessage(data []byte) (err error) {
	var tmp map[string]interface{}

	if err = crypto.ParseMessage(data, config.ServerPublicKey, config.ClientPrivateKey, &tmp); err != nil {
		return
	}

	if msg, exists := tmp["message"]; exists {
		log.Println(msg)
	}

	return
}

func parseErrorMessage(data []byte) (err error) {
	var tmp map[string]interface{}

	if err = crypto.ParseMessage(data, config.ServerPublicKey, config.ClientPrivateKey, &tmp); err != nil {
		return
	}

	if msg, exists := tmp["message"]; exists {
		err = errors.New(msg.(string))
	} else {
		err = errors.New("unknown server error")
	}

	return
}

//parseMessage securely parses a received message sending it to the registered handler and sending the response or error
func parseMessage(data []byte, messageType MessageType) {
	var err error

	defer func() {
		if err != nil {
			errChan <- err
		}
	}()

	switch messageType {
	case MessageTypeInsecure:
		return
	case MessageTypeCommand:
		err = parseCommandMessage(data)
	case MessageTypeInfo:
		err = parseInfoMessage(data)
	case MessageTypeError:
		err = parseErrorMessage(data)
		log.Fatalln(err)
		return
	}

	return
}

//verifyPinnedCertificate if enable pinning is true this will verify that the server has provided the pinned certificate using SHA256 hashes
func verifyPinnedCertificate() func(rawCerts [][]byte, verifiedChains [][]*x509.Certificate) error {
	if !config.EnableCertPin {
		return nil
	}

	return func(rawCerts [][]byte, verifiedChains [][]*x509.Certificate) error {
		for _, rawCert := range rawCerts {
			hash, err := crypto.HashToString(rawCert)

			if err != nil {
				return err
			}

			if hash == config.CertFingerprint {
				return nil
			}
		}

		return ErrPinnedCertNotFound
	}
}

//UInt16ToByte converts a uint16 to the little endian []byte representation
func UInt16ToByte(val uint16) []byte {
	d := make([]byte, 2)

	binary.LittleEndian.PutUint16(d, val)

	return d
}

//UInt32ToByte converts a uint32 to the little endian []byte representation
func UInt32ToByte(val uint32) []byte {
	d := make([]byte, 4)

	binary.LittleEndian.PutUint32(d, val)

	return d
}

//ReadFrame reads a frame from the specified reader
func ReadFrame(r *bufio.Reader, maxSize ByteSize) (data []byte, err error) {
	sizeBytes := make([]byte, 4)

	if _, err = io.ReadFull(r, sizeBytes); err != nil {
		return
	}

	msgSize := binary.LittleEndian.Uint32(sizeBytes)

	if uint64(msgSize) > uint64(maxSize) {
		if _, err = r.Discard(int(msgSize)); err != nil {
			return
		}

		err = ErrRequestSize

		return
	}

	data = make([]byte, msgSize)

	_, err = io.ReadFull(r, data)

	return
}

//Close immediately stops incoming message handling. However, messages may still be sent out.
func Close() {
	closing <- true
}

//Connect creates a new connection to the specified server
func Connect() (err error) {
	dialer := &net.Dialer{
		Timeout:   30 * time.Second,
		KeepAlive: 10 * time.Minute,
		DualStack: true,
	}

	if conn, err = tls.DialWithDialer(dialer, "tcp", config.ServerAddr, &tls.Config{
		InsecureSkipVerify:    config.DisableCertVerification,
		VerifyPeerCertificate: verifyPinnedCertificate(),
	}); err != nil {
		return
	}

	go handleMessages()
	go tcpReadPump(conn)

	if err = SendMessage(types.ResponseBase{
		Type:   types.MessageConnected,
		Status: types.ResponseStatusSuccess,
	}); err != nil {
		errChan <- err
	}

	for ch := range connectionNotifiers {
		go func(ch chan bool) {
			ch <- true
		}(ch)
	}

	err = <-errChan
	closing <- true

	return
}

func tcpReadPump(conn *tls.Conn) {
	reader := bufio.NewReader(conn)

	for {
		data, err := ReadFrame(reader, Megabyte*5)

		if err != nil {
			errChan <- err
			return
		}

		receive <- data
	}
}

//SendMessage sends a signed message to the server
func SendMessage(message interface{}) (err error) {
	if conn == nil {
		err = ErrNotConnected
		return
	}

	data, err := json.Marshal(message)

	if err != nil {
		return
	}

	encrypted, err := crypto.EncryptMessage(data, config.ServerPublicKey, config.ClientPrivateKey)

	if err != nil {
		return
	}

	write(encrypted, MessageTypeCommand)

	return
}

func write(p []byte, messageType MessageType) (err error) {
	m := append(protocolVersion, append(UInt16ToByte(uint16(messageType)), p...)...)
	l := uint32(len(m))
	m = append(UInt32ToByte(l), m...)

	_, err = conn.Write(m)

	return
}

//AddHandler adds a secure message handler of the specified type to the connection.
func AddHandler(commandType string, handlerFunc ConnHandlerFunc) {
	handlers[strings.ToLower(commandType)] = handlerFunc
}

//RegisterConnectionNotifier RegisterConnectionNotifier
func RegisterConnectionNotifier(ch chan bool) {
	notifierLock.Lock()

	defer notifierLock.Unlock()

	connectionNotifiers[ch] = true
}

//UnregisterConnectionNotifier UnregisterConnectionNotifier
func UnregisterConnectionNotifier(ch chan bool) {
	notifierLock.Lock()

	defer notifierLock.Unlock()

	delete(connectionNotifiers, ch)
}
