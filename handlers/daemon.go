package handlers

import (
	"gitlab.com/siacentral/client/methods"
	"gitlab.com/siacentral/client/types"
	"gitlab.com/siacentral/siadaemon"
)

//handleGetOutput handleGetOutput
func handleGetOutput(data map[string]interface{}) interface{} {
	resp := methods.GetDaemonStatus()

	return resp
}

//handleRestartDaemon handleRestartDaemon
func handleRestartDaemon(data map[string]interface{}) interface{} {
	var resp types.DaemonStatusResponse

	resp.Type = types.MessageDaemonStart

	if err := siadaemon.Restart(); err != nil {
		resp.Status = types.ResponseStatusErr
		resp.Message = err.Error()
		return resp
	}

	resp = methods.GetDaemonStatus()
	resp.Message = "user triggered restart"

	return resp
}
