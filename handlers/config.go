package handlers

import (
	"log"

	"gitlab.com/siacentral/client/types"
	"gitlab.com/siacentral/client/utils"
	"gitlab.com/siacentral/client/methods"
)

//handleGetConfig gets the current host configuration from the sia daemon
func handleGetConfig(data map[string]interface{}) interface{} {
	return methods.GetHostSettings()
}

//handleUpdateConfig updates the host config to the specified values
func handleUpdateConfig(data map[string]interface{}) interface{} {
	var request types.UpdateHostConfigRequest

	if err := utils.UnmarshalMap(data, &request); err != nil {
		log.Println(err)

		resp := types.ResponseBase{
			Status:  types.ResponseStatusErr,
			Message: "error parsing message",
		}

		return resp
	}

	return methods.UpdateHostConfig(request)
}

//handleAnnounceHost announces the host to the network
func handleAnnounceHost(data map[string]interface{}) interface{} {
	var request types.AnnounceRequest

	if err := utils.UnmarshalMap(data, &request); err != nil {
		log.Println(err)

		resp := types.ResponseBase{
			Status:  types.ResponseStatusErr,
			Message: "error parsing message",
		}

		return resp
	}

	return methods.AnnounceHost(request.Address)
}
