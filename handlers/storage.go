package handlers

import (
	"io/ioutil"
	"os"
	"path/filepath"

	"github.com/shirou/gopsutil/disk"
	"gitlab.com/siacentral/client/methods"
	"gitlab.com/siacentral/client/types"
	"gitlab.com/siacentral/client/utils"
)

//handleListDir lists all files and folders in the specified directory of the host pc. Used to add a new storage folder in the Sia Daemon
func handleListDir(data map[string]interface{}) interface{} {
	var request types.DirectoryRequest
	var resp types.DirectoryResponse
	var err error

	if err = utils.UnmarshalMap(data, &request); err != nil {
		resp.Status = types.ResponseStatusErr
		resp.Message = err.Error()
		return resp
	}

	statPath := request.DirPath

	if len(statPath) == 0 {
		var ex string

		ex, err = os.Executable()

		if err != nil {
			resp.Status = types.ResponseStatusErr
			resp.Message = err.Error()
			return resp
		}

		statPath = filepath.Dir(ex)
	}

	if statPath, err = filepath.Abs(statPath); err != nil {
		resp.Status = types.ResponseStatusErr
		resp.Message = err.Error()
		return resp
	}

	parentPath, err := filepath.Abs(filepath.Join(statPath, ".."))

	if err != nil {
		resp.Status = types.ResponseStatusErr
		resp.Message = err.Error()
		return resp
	}

	folders, err := ioutil.ReadDir(statPath)

	if err != nil {
		resp.Status = types.ResponseStatusErr
		resp.Message = err.Error()
		return resp
	}

	for _, file := range folders {
		if !file.IsDir() {
			continue
		}

		fullPath := filepath.Join(statPath, file.Name())

		resp.Directories = append(resp.Directories, types.DirectoryStat{
			Name: file.Name(),
			Path: fullPath,
		})
	}

	totalUsage, err := disk.Usage(statPath)

	if err != nil {
		resp.Status = types.ResponseStatusErr
		resp.Message = err.Error()
		return resp
	}

	resp.FreeBytes = totalUsage.Free
	resp.TotalBytes = totalUsage.Total
	resp.UsedBytes = resp.TotalBytes - resp.FreeBytes
	resp.DirPath = statPath
	resp.ParentPath = parentPath

	resp.Status = types.ResponseStatusSuccess
	resp.Message = "successfully got directories"

	return resp
}

//handleGetContracts gets all storage obligations managed by the host
func handleGetContracts(data map[string]interface{}) interface{} {
	return methods.GetHostContracts()
}

//handleGetStorageFolders gets all of the storage folders configured on the host
func handleGetStorageFolders(data map[string]interface{}) interface{} {
	return methods.GetStorageFolders()
}

//handleAddLocalFolder adds a local folder at the specified path
func handleAddLocalFolder(data map[string]interface{}) interface{} {
	var err error
	var request types.DirectoryRequest
	var resp types.DirectoryResponse

	if err = utils.UnmarshalMap(data, &request); err != nil {
		resp.Status = types.ResponseStatusErr
		resp.Message = err.Error()
		return resp
	}

	if err = os.MkdirAll(request.DirPath, 755); err != nil {
		resp.Status = types.ResponseStatusErr
		resp.Message = err.Error()
		return resp
	}

	return handleListDir(data)
}

//handleAddStorageFolder adds a storage folder to the sia daemon
func handleAddStorageFolder(data map[string]interface{}) interface{} {
	var request types.FolderRequest
	var resp types.StorageFoldersResponse

	if err := utils.UnmarshalMap(data, &request); err != nil {
		resp.Status = types.ResponseStatusErr
		resp.Message = "error parsing message"

		return resp
	}

	resp = methods.AddStorageFolder(request)

	return resp
}

//handleResizeStorageFolder resizes an existing folder in the siadaemon
func handleResizeStorageFolder(data map[string]interface{}) interface{} {
	var request types.FolderRequest
	var resp types.StorageFoldersResponse

	if err := utils.UnmarshalMap(data, &request); err != nil {
		resp.Status = types.ResponseStatusErr
		resp.Message = "error parsing message"

		return resp
	}

	resp = methods.ResizeStorageFolder(request)

	return resp
}

//handleRemoveStorageFolder removes a storage folder from the sia daemon
func handleRemoveStorageFolder(data map[string]interface{}) interface{} {
	var request types.FolderRequest
	var resp types.StorageFoldersResponse

	if err := utils.UnmarshalMap(data, &request); err != nil {
		resp.Status = types.ResponseStatusErr
		resp.Message = "error parsing message"

		return resp
	}

	resp = methods.RemoveStorageFolder(request)

	return resp
}
