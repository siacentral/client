package handlers

import (
	"gitlab.com/siacentral/client/types"
	"gitlab.com/siacentral/client/connection"
)

var endpoints = map[types.MessageType]connection.ConnHandlerFunc{
	//Data Handlers
	types.MessageDataListContracts: handleGetContracts,

	//Storage Handlers
	types.MessageAddLocalFolder:      handleAddLocalFolder,
	types.MessageStorageListDir:      handleListDir,
	types.MessageStorageListFolders:  handleGetStorageFolders,
	types.MessageStorageAddFolder:    handleAddStorageFolder,
	types.MessageStorageResizeFolder: handleResizeStorageFolder,
	types.MessageStorageRemoveFolder: handleRemoveStorageFolder,

	//Host Handlers
	types.MessageHostStatus:       handleHostStatus,
	types.MessageHostConfig:       handleGetConfig,
	types.MessageHostUpdateConfig: handleUpdateConfig,
	types.MessageHostAnnounce:     handleAnnounceHost,

	//Sia daemon Handlers
	types.MessageDaemonStatus: handleGetOutput,
	types.MessageDaemonStart:  handleRestartDaemon,
}

//AddConnectionHandlers adds all supported handlers to the connection
func AddConnectionHandlers() {
	for requestType, handler := range endpoints {
		connection.AddHandler(string(requestType), handler)
	}
}
