package handlers

import (
	"log"
	"time"

	"gitlab.com/siacentral/client/connection"
	"gitlab.com/siacentral/client/methods"
	"gitlab.com/siacentral/client/types"
)

//handleHostStatus handleHostStatus
func handleHostStatus(data map[string]interface{}) interface{} {
	resp := methods.GetHostStatus()

	return resp
}

//SyncHostStats gets the latest host statistics and syncs it to the server
func SyncHostStats() {
	statStart := time.Now()

	defer func() {
		log.Printf("Synced host statistics in %s", time.Since(statStart))
	}()

	resp := methods.GetHostMetrics()

	if resp.Status != types.ResponseStatusSuccess {
		log.Println(resp.Message)
		return
	}

	if err := connection.SendMessage(&resp); err != nil {
		log.Println(err)
		return
	}
}
